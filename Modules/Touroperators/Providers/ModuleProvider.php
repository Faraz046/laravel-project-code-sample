<?php

namespace TypiCMS\Modules\Touroperators\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\FileObserver;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Core\Services\Cache\LaravelCache;
use TypiCMS\Modules\Touroperators\Models\Touroperator;
use TypiCMS\Modules\Touroperators\Models\TouroperatorTranslation;
use TypiCMS\Modules\Touroperators\Repositories\CacheDecorator;
use TypiCMS\Modules\Touroperators\Repositories\EloquentTouroperator;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'typicms.touroperators'
        );

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['touroperators' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'touroperators');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'touroperators');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/touroperators'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../database' => base_path('database'),
        ], 'migrations');

        AliasLoader::getInstance()->alias(
            'Touroperators',
            'TypiCMS\Modules\Touroperators\Facades\Facade'
        );

        // Observers
        TouroperatorTranslation::observe(new SlugObserver());
        Touroperator::observe(new FileObserver());
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register('TypiCMS\Modules\Touroperators\Providers\RouteServiceProvider');

        /*
         * Sidebar view composer
         */
        $app->view->composer('core::admin._sidebar', 'TypiCMS\Modules\Touroperators\Composers\SidebarViewComposer');

        /*
         * Add the page in the view.
         */
        $app->view->composer('touroperators::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('touroperators');
        });

        $app->bind('TypiCMS\Modules\Touroperators\Repositories\TouroperatorInterface', function (Application $app) {
            $repository = new EloquentTouroperator(new Touroperator());
            if (!config('typicms.cache')) {
                return $repository;
            }
            $laravelCache = new LaravelCache($app['cache'], 'touroperators', 10);

            return new CacheDecorator($repository, $laravelCache);
        });
    }
}
