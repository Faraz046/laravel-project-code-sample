<?php

namespace TypiCMS\Modules\Touroperators\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use TypiCMS\Modules\Core\Facades\TypiCMS;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TypiCMS\Modules\Touroperators\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {

            /*
             * Front office routes
             */
            if ($page = TypiCMS::getPageLinkedToModule('touroperators')) {
                $options = $page->private ? ['middleware' => 'auth'] : [];
                foreach (config('translatable.locales') as $lang) {
                    if ($page->translate($lang)->status && $uri = $page->uri($lang)) {
                        $router->get($uri, $options + ['as' => $lang.'.touroperators', 'uses' => 'PublicController@index']);
                        $router->get($uri.'/{slug}', $options + ['as' => $lang.'.touroperators.slug', 'uses' => 'PublicController@show']);
                    }
                }
            }

            /*
             * Admin routes
             */
            $router->get('admin/touroperators', ['as' => 'admin.touroperators.index', 'uses' => 'AdminController@index']);
            $router->get('admin/touroperators/create', ['as' => 'admin.touroperators.create', 'uses' => 'AdminController@create']);
            $router->get('admin/touroperators/{touroperator}/edit', ['as' => 'admin.touroperators.edit', 'uses' => 'AdminController@edit']);
            $router->post('admin/touroperators', ['as' => 'admin.touroperators.store', 'uses' => 'AdminController@store']);
            $router->put('admin/touroperators/{touroperator}', ['as' => 'admin.touroperators.update', 'uses' => 'AdminController@update']);

            /*
             * API routes
             */
            $router->get('api/touroperators', ['as' => 'api.touroperators.index', 'uses' => 'ApiController@index']);
            $router->put('api/touroperators/{touroperator}', ['as' => 'api.touroperators.update', 'uses' => 'ApiController@update']);
            $router->delete('api/touroperators/{touroperator}', ['as' => 'api.touroperators.destroy', 'uses' => 'ApiController@destroy']);
        });
    }
}
