<?php

namespace TypiCMS\Modules\Touroperators\Models;

use TypiCMS\Modules\Core\Models\BaseTranslation;

class TouroperatorTranslation extends BaseTranslation
{
    /**
     * get the parent model.
     */
    public function owner()
    {
        return $this->belongsTo('TypiCMS\Modules\Touroperators\Models\Touroperator', 'touroperator_id');
    }
}
