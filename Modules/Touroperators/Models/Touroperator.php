<?php

namespace TypiCMS\Modules\Touroperators\Models;

use Auth;
use DB;
use Laracasts\Presenter\PresentableTrait;
use TypiCMS\Modules\Companies\Models\Company;
use TypiCMS\Modules\Core\Models\Base;
use TypiCMS\Modules\History\Traits\Historable;

class Touroperator extends Base
{
    use Historable;
    use PresentableTrait;

    /**
     * Presenter class
     *
     * @var string
     */
    protected $presenter = 'TypiCMS\Modules\Touroperators\Presenters\ModulePresenter';
    
    /**
     * Declare any properties that should be hidden from JSON Serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['company'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'company_id',
        'code',
        'active',
        'country',
        'position',
        'status',
        'image',
    ];

    /**
     * Translatable model configs.
     *
     * @var array
     */
    public $translatedAttributes = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['status', 'title', 'thumb'];

    /**
     * Columns that are file.
     *
     * @var array
     */
    public $attachments = [
        'image',
    ];

    public static function boot()
    {
        static::creating(function($model) {
            if(is_null($model->company_id) && (bool)Auth::user()->owner === false && ! is_null(Auth::user()->company_id)) {
                $model->company_id = Auth::user()->company_id;
            }

            $model->position = Touroperator::where('company_id', $model->company_id)->max('position') + 1;
        });

        static::saving(function(Touroperator $model) {
            if((int)$model->position !== (int)$model->getOriginal('position')) {
                $newPosition = (int)$model->position;
                $oldPosition = (int)$model->getOriginal('position');

                if($newPosition > $oldPosition) {
                    Touroperator::where('company_id', $model->company_id)
                           ->where('position', '>', $oldPosition)
                           ->where('position', '<=', $newPosition)
                           ->where('position', '>', 0)
                           ->update(['position' => DB::raw('position - 1')]);
                } else {
                    Touroperator::where('company_id', $model->company_id)
                           ->where('position', '<', $oldPosition)
                           ->where('position', '>=', $newPosition)
                           ->update(['position' => DB::raw('position + 1')]);
                }
            }
        });
    }

    /**
     * Getter for status attribute
     * @param $value
     * @return mixed
     */
    public function getStatusAttribute($value)
    {
        return $this->active;
    }

    /**
     * Setter for status attribute
     * @param $value
     */
    public function setStatusAttribute($value)
    {
        $this->attributes['active'] = $value;
    }

    /**
     * Getter for title attribute
     * @param $value
     * @return mixed
     */
    public function getTitleAttribute($value)
    {
        return $this->name;
    }

    /**
     * Company Relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
