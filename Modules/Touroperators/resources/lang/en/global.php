<?php

return [
    'name'     => 'Touroperators',
    'touroperators'  => 'touroperator|touroperators',
    'New'      => 'New touroperator',
    'Edit'     => 'Edit touroperator',
    'Back'     => 'Back to touroperators',
];
