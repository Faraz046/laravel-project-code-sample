<?php

return [
    'name'     => 'Touroperators',
    'touroperators'  => 'touroperator|touroperators',
    'New'      => 'Nouveau touroperator',
    'Edit'     => 'Modifier touroperator',
    'Back'     => 'Retour à la liste des touroperators',
];
