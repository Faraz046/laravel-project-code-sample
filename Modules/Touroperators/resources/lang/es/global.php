<?php

return [
    'name'     => 'Objetos',
    'touroperators'  => 'objeto|objetos',
    'New'      => 'Nuevo objeto',
    'Edit'     => 'Editar objeto',
    'Back'     => 'Volver a los objetos',
];
