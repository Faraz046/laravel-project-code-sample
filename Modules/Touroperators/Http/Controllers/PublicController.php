<?php

namespace TypiCMS\Modules\Touroperators\Http\Controllers;

use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Touroperators\Repositories\TouroperatorInterface;

class PublicController extends BasePublicController
{
    public function __construct(TouroperatorInterface $touroperator)
    {
        parent::__construct($touroperator);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all();

        return view('touroperators::public.index')
            ->with(compact('models'));
    }

    /**
     * Show news.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $model = $this->repository->bySlug($slug);

        return view('touroperators::public.show')
            ->with(compact('model'));
    }
}
