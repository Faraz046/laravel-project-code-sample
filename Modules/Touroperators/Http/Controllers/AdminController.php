<?php

namespace TypiCMS\Modules\Touroperators\Http\Controllers;

use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Touroperators\Http\Requests\FormRequest;
use TypiCMS\Modules\Touroperators\Models\Touroperator;
use TypiCMS\Modules\Touroperators\Repositories\TouroperatorInterface;

class AdminController extends BaseAdminController
{
    /**
     * AdminController constructor.
     * @param \TypiCMS\Modules\Touroperators\Repositories\TouroperatorInterface $touroperator
     */
    public function __construct(TouroperatorInterface $touroperator)
    {
        parent::__construct($touroperator);
    }

    /**
     * Create form for a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = $this->repository->getModel();
        $positions = $this->generatePositionsArray(object_get(Auth::user(), 'company_id'), true);

        return view('core::admin.create')
            ->with(compact('model'));
    }

    /**
     * Edit form for the specified resource.
     *
     * @param \TypiCMS\Modules\Touroperators\Models\Touroperator $touroperator
     *
     * @return \Illuminate\View\View
     */
    public function edit(Touroperator $touroperator)
    {
        return view('core::admin.edit')
            ->with([
                'model' => $touroperator,
                'positions' => $this->generatePositionsArray($touroperator->company_id)
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \TypiCMS\Modules\Touroperators\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $touroperator = $this->repository->create($request->all());

        return $this->redirect($request, $touroperator);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \TypiCMS\Modules\Touroperators\Models\Touroperator            $touroperator
     * @param \TypiCMS\Modules\Touroperators\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Touroperator $touroperator, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $touroperator);
    }

    /**
     * Generate positions array
     * @param $companyId
     * @param bool $new
     * @return array
     */
    private function generatePositionsArray($companyId, $new = false)
    {
        if($companyId) {
            $max = $this->repository->getModel()->where('company_id', $companyId)->max('position') + 1;
        } else {
            $max = $this->repository->getModel()->whereNull('company_id')->max('position') + 1;
        }

        if($new === true) {
            $max += 1;
        }

        $data = [];

        for($i = 1; $i < $max; ++$i) {
            $data[$i] = $i;
        }

        return $data;
    }
}
