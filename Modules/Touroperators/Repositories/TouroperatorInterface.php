<?php

namespace TypiCMS\Modules\Touroperators\Repositories;

use TypiCMS\Modules\Core\Repositories\RepositoryInterface;

interface TouroperatorInterface extends RepositoryInterface
{
}
