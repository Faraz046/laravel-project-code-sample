<?php

namespace TypiCMS\Modules\Touroperators\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;

class EloquentTouroperator extends RepositoriesAbstract implements TouroperatorInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
