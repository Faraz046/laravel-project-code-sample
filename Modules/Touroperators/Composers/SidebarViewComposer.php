<?php

namespace TypiCMS\Modules\Touroperators\Composers;

use Illuminate\Contracts\View\View;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;
use TypiCMS\Modules\Core\Composers\BaseSidebarViewComposer;

class SidebarViewComposer extends BaseSidebarViewComposer
{
    public function compose(View $view)
    {
        $view->sidebar->group(trans('global.menus.filters'), function (SidebarGroup $group) {
            $group->addItem(trans('touroperators::global.name'), function (SidebarItem $item) {
                $item->icon = config('typicms.touroperators.sidebar.icon');
                $item->weight = config('typicms.touroperators.sidebar.weight');
                $item->route('admin.touroperators.index');
                $item->append('admin.touroperators.create');
                $item->authorize(
                    $this->auth->hasAccess('touroperators.index')
                );
            });
        });
    }
}
