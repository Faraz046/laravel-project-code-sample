<?php

return [
    'name'                            => 'Menu',
    'menus'                           => 'menu|menu',
    'New'                             => 'Nové menu',
    'Edit'                            => 'Editovať menu',
    'Back'                            => 'Späť na menu',
    'No menu found with name “:name”' => 'Menu s názvom “:name” sa nenašlo.',
    'Active tab'                      => 'Aktivna záložka',
    'New tab'                         => 'Nová záložka',
    'New menulink'                    => 'Nový odkaz v menu',
    'Edit menulink'                   => 'Editovať odkaz v menu',
    'Back to menu'                    => 'Späť na menu',
];
