<?php

namespace TypiCMS\Modules\Menus\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;

class EloquentMenulink extends RepositoriesAbstract implements MenulinkInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get a menu’s items and children.
     *
     * @param int $menuId
     * @param bool $all published or all
     * @param int|null $idToExclude
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allFromMenu($menuId = null, $all = false, $idToExclude = null)
    {
        $query = $this->model->with('translations')
            ->order()
            ->where('menu_id', $menuId);

        // All posts or only published
        if (!$all) {
            $query->where('status', 1);
        }

        if(! is_null($idToExclude)) {
            $query->where('id', '!=', $idToExclude);
        }

        $models = $query->get();

        return $models;
    }

    /**
     * Get sort data.
     *
     * @param int   $position
     * @param array $item
     *
     * @return array
     */
    protected function getSortData($position, $item)
    {
        return [
            'position'  => $position,
            'parent_id' => $item['parent_id'],
        ];
    }
}
