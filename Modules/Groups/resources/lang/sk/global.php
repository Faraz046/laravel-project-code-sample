<?php

return [
    'name'              => 'Role',
    'groups'            => 'rola|role',
    'New'               => 'Nová rola',
    'Edit'              => 'Editovať rolu',
    'Back'              => 'Späť na role',
    'Group permissions' => 'Práva pre rolu',
];
