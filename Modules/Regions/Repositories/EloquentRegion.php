<?php

namespace TypiCMS\Modules\Regions\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;

class EloquentRegion extends RepositoriesAbstract implements RegionInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
