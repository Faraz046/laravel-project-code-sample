<?php

namespace TypiCMS\Modules\Regions\Repositories;

use TypiCMS\Modules\Core\Repositories\RepositoryInterface;

interface RegionInterface extends RepositoryInterface
{
}
