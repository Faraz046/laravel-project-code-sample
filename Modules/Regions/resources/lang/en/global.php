<?php

return [
    'name'     => 'Regions',
    'regions'  => 'region|regions',
    'New'      => 'New region',
    'Edit'     => 'Edit region',
    'Back'     => 'Back to regions',
];
