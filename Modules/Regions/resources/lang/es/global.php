<?php

return [
    'name'     => 'Objetos',
    'regions'  => 'objeto|objetos',
    'New'      => 'Nuevo objeto',
    'Edit'     => 'Editar objeto',
    'Back'     => 'Volver a los objetos',
];
