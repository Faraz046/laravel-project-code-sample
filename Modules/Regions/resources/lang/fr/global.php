<?php

return [
    'name'     => 'Regions',
    'regions'  => 'region|regions',
    'New'      => 'Nouveau region',
    'Edit'     => 'Modifier region',
    'Back'     => 'Retour à la liste des regions',
];
