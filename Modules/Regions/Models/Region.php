<?php

namespace TypiCMS\Modules\Regions\Models;

use Auth;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Laracasts\Presenter\PresentableTrait;
use TypiCMS\Modules\Core\Models\Base;

class Region extends Base
{
    use PresentableTrait;

    /**
     * @var string3
     */
    protected $presenter = 'TypiCMS\Modules\Regions\Presenters\ModulePresenter';
    
    /**
     * Declare any properties that should be hidden from JSON Serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'table',
        'parent_id',
        'category_id',
        'position'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'thumb',
        'text',
        'children',
        'li_attr'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'suggester_item';

    protected static $primaryTable = 'traveldata_suggester_item';
    protected static $translationTable = 'traveldata_suggester_item_language';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'date_created';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'date_modified';

    /**
     * The "booting" method of the model.
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('company_id', function(Builder $query)
        {
            if(! Auth::check()) {
                return $query;
            }
        });

        static::deleting(function($model) {
            //
        });
    }

    /**
     * Mock historable method
     */
    public function writeHistory()
    {
        //
    }

    /**
     * Clear string from given pattern
     * @param $table
     * @return mixed
     */
    private static function prepareTable($table)
    {
        return str_replace('traveldata_', '', $table);
    }

    /**
     * Override table where to make query from
     * @return mixed|string
     */
    public function getTable()
    {
        if(Auth::user()->owner === true)
            return parent::getTable();

        return static::prepareTable(static::$primaryTable . '_' . Auth::id());
    }

    /**
     * Accessor for title attribute
     * @param $value
     * @return mixed
     */
    public function getTitleAttribute($value)
    {
        return array_get($this->attributes, 'title');
    }

    /**
     * Accessor for text attribute
     * @return mixed
     */
    public function getTextAttribute()
    {
        return array_get($this->attributes, 'title');
    }

    /**
     * Accessor for children attribute
     * @return bool
     */
    public function getChildrenAttribute()
    {
        return (bool)array_get($this->attributes, 'has_childs');
    }

    /**
     * Accessor for li_attr attribute
     * @return array
     */
    public function getLiAttrAttribute()
    {
        return [
            'data-active' => array_get($this->attributes, 'active'),
            'data-category-id' => array_get($this->attributes, 'category_id'),
            'data-category-position' => array_get($this->attributes, 'category_position'),
            'data-category-type' => array_get($this->attributes, 'category_type'),
            'data-custom' => array_get($this->attributes, 'custom'),
            'data-original-id' => array_get($this->attributes, 'original_id')
        ];
    }

    /**
     * Scope filter for regions category
     * @param $query
     * @return mixed
     */
    public function scopeRegion($query)
    {
        return $query->where('category_id', 1);
    }

    /**
     * Scope filter for destinations category
     * @param $query
     * @return mixed
     */
    public function scopeDestination($query)
    {
        return $query->where('category_id', 2);
    }

    /**
     * Scope filter for cities category
     * @param $query
     * @return mixed
     */
    public function scopeCity($query)
    {
        return $query->where('category_id', 3);
    }

    /**
     * Scope filter for hotels category
     * @param $query
     * @return mixed
     */
    public function scopeHotel($query)
    {
        return $query->where('category_id', 4);
    }

    /**
     * Append table string
     * @param $table
     * @return string
     */
    protected static function appendTable($table)
    {
        if(Auth::check() && Auth::user()->owner === true)
            return $table;

        return $table . '_' . Auth::id();
    }

    /**
     * Scope helper for index query
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return mixed
     */
    public function scopeIndex(Builder $query)
    {
        $query->select([
            '*',
            DB::raw("(select name from " . static::appendTable(static::$translationTable) . " where item_id = traveldata_" . (new self)->getTable() . ".id and language_id=1) as title"),
            DB::raw("(select position from traveldata_suggester_category where category_id = traveldata_suggester_category.id) as category_position"),
            DB::raw("(select code from traveldata_suggester_category where category_id = traveldata_suggester_category.id) as category_type"),
            DB::raw("(select seo_title from " . static::appendTable(static::$translationTable) . " where item_id = traveldata_" . (new self)->getTable() . ".id and language_id=1) as seo_title"),
            DB::raw("(select seo_description from " . static::appendTable(static::$translationTable) . " where item_id = traveldata_" . (new self)->getTable() . ".id and language_id=1) as seo_description"),
            DB::raw("(select seo_keywords from " . static::appendTable(static::$translationTable) . " where item_id = traveldata_" . (new self)->getTable() . ".id and language_id=1) as seo_keywords"),
            DB::raw("(select seo_url from " . static::appendTable(static::$translationTable) . " where item_id = traveldata_" . (new self)->getTable() . ".id and language_id=1) as seo_url"),
            DB::raw("(select flight_duration from " . static::appendTable(static::$translationTable) . " where item_id = traveldata_" . (new self)->getTable() . ".id and language_id=1) as flight_duration"),
            DB::raw("(select description from " . static::appendTable(static::$translationTable) . " where item_id = traveldata_" . (new self)->getTable() . ".id and language_id=1) as description"),
        ]);

        return $query;
    }

    /**
     * Helper for handling search query
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param String $keyword
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch(Builder $query, $keyword)
    {
        $query->select([
            static::prepareTable(static::appendTable(static::$primaryTable)) . '.id',
            'suggester_category.name as category_type',
            static::prepareTable(static::appendTable(static::$translationTable)) . '.name as name',
            static::prepareTable(static::appendTable(static::$translationTable)) . '.escaped_name'
        ]);

        $query->join('suggester_category', 'suggester_category.id', '=', static::prepareTable(static::appendTable(static::$primaryTable)) . '.category_id');
        $query->join(static::prepareTable(static::appendTable(static::$translationTable)), function($join)
        {
            $join->on(static::appendTable(static::prepareTable(static::$translationTable)) . '.item_id', '=', static::prepareTable(static::appendTable(static::$primaryTable)) . '.id');
            $join->where('language_id', '=', 1);
        });

        $query->where(static::appendTable(static::prepareTable(static::$translationTable)) . '.escaped_name', 'LIKE', "%$keyword%");

        return $query;
    }

    /**
     * Recursive function which returns item parents ids
     * @param null $itemId
     * @param array $parents
     * @return array
     */
    public function getParents($itemId = null, array $parents = [])
    {
        if($itemId === null) {
            $itemId = $this->id;
        }

        $region = Region::find($itemId);
        $parentId = $region->parent_id;

        if($parentId)
        {
            array_unshift($parents, $parentId);
            return $this->getParents($parentId, $parents);
        }


        return $parents;
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function translation()
    {
        return $this->hasOne('TypiCMS\Modules\Regions\Models\RegionTranslation', 'item_id');
    }
}
