<?php

namespace TypiCMS\Modules\Regions\Models;

use Auth;
use Illuminate\Database\Eloquent\Builder;
use TypiCMS\Modules\Core\Models\BaseTranslation;

class RegionTranslation extends BaseTranslation
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'suggester_item_language';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'escaped_name',
        'item_id',
        'locale',
        'language_id',
        'description',
        'flight_duration',
        'seo_title',
        'seo_url',
        'seo_description',
        'seo_keywords',
    ];

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = null;

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = null;

    /**
     * Table
     * @var string
     */
    protected static $primaryTable = 'traveldata_suggester_item_language';

    public function writeHistory($action, $title = null, $locale = null)
    {
        //
    }

    public function present()
    {
        //
    }

    /**
     * Booting model
     */
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('company', function(Builder $query)
        {
            if(! Auth::check()) {
                return $query;
            }

            if(Auth::user()->owner === false) {
                $query->from(static::prepareTable((new self)->getTable()));
            }
        });

        static::saving(function($record) {
            if(empty($record->description)) {
                $record->description = '';
            }

            if(empty($record->flight_duration)) {
                $record->flight_duration = '';
            }

            if(empty($record->seo_title)) {
                $record->seo_title = '';
            }

            if(empty($record->seo_url)) {
                $record->seo_url = '';
            }

            if(empty($record->seo_description)) {
                $record->seo_description = 0;
            }

            if(empty($record->seo_keywords)) {
                $record->seo_keywords = 0;
            }

            if(empty($record->language_id)) {
                $record->language_id = 1;
            }

            if(empty($record->locale)) {
                $record->locale = 'sk';
            }
        });

        static::deleting(function($record) {
            //
        });
    }

    public static function prepareTable($table)
    {
        return str_replace('traveldata_', '', $table);
    }

    public function getTable()
    {
        if(Auth::check() && Auth::user()->owner === true) {
            return parent::getTable();
        }

        return static::prepareTable(static::$primaryTable . '_' . Auth::id());
    }

    /**
     * get the parent model.
     */
    public function owner()
    {
        return $this->belongsTo('TypiCMS\Modules\Regions\Models\Region', 'item_id');
    }

    public function setSlugAttribute($value)
    {
        //
    }
}
