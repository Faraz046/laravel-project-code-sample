<?php

namespace TypiCMS\Modules\Regions\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\FileObserver;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Core\Services\Cache\LaravelCache;
use TypiCMS\Modules\Regions\Models\Region;
use TypiCMS\Modules\Regions\Models\RegionTranslation;
use TypiCMS\Modules\Regions\Repositories\CacheDecorator;
use TypiCMS\Modules\Regions\Repositories\EloquentRegion;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'typicms.regions'
        );

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['regions' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'regions');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'regions');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/regions'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../database' => base_path('database'),
        ], 'migrations');

        AliasLoader::getInstance()->alias(
            'Regions',
            'TypiCMS\Modules\Regions\Facades\Facade'
        );

        // Observers
        RegionTranslation::observe(new SlugObserver());
        Region::observe(new FileObserver());
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register('TypiCMS\Modules\Regions\Providers\RouteServiceProvider');

        /*
         * Sidebar view composer
         */
        $app->view->composer('core::admin._sidebar', 'TypiCMS\Modules\Regions\Composers\SidebarViewComposer');

        /*
         * Add the page in the view.
         */
        $app->view->composer('regions::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('regions');
        });

        $app->bind('TypiCMS\Modules\Regions\Repositories\RegionInterface', function (Application $app) {
            $repository = new EloquentRegion(new Region());
            if (!config('typicms.cache')) {
                return $repository;
            }
            $laravelCache = new LaravelCache($app['cache'], 'regions', 10);

            return new CacheDecorator($repository, $laravelCache);
        });
    }
}
