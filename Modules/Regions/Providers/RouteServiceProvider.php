<?php

namespace TypiCMS\Modules\Regions\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use TypiCMS\Modules\Core\Facades\TypiCMS;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TypiCMS\Modules\Regions\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {

            /*
             * Front office routes
             */
            if ($page = TypiCMS::getPageLinkedToModule('regions')) {
                $options = $page->private ? ['middleware' => 'auth'] : [];
                foreach (config('translatable.locales') as $lang) {
                    if ($page->translate($lang)->status && $uri = $page->uri($lang)) {
                        $router->get($uri, $options + ['as' => $lang.'.regions', 'uses' => 'PublicController@index']);
                        $router->get($uri.'/{slug}', $options + ['as' => $lang.'.regions.slug', 'uses' => 'PublicController@show']);
                    }
                }
            }

            /*
             * Admin routes
             */
            $router->get('admin/regions', ['as' => 'admin.regions.index', 'uses' => 'AdminController@index']);
            $router->get('admin/regions/create', ['as' => 'admin.regions.create', 'uses' => 'AdminController@create']);
            $router->get('admin/regions/{region}/edit', ['as' => 'admin.regions.edit', 'uses' => 'AdminController@edit']);
            $router->post('admin/regions', ['as' => 'admin.regions.store', 'uses' => 'AdminController@store']);
            $router->put('admin/regions/{region}', ['as' => 'admin.regions.update', 'uses' => 'AdminController@update']);

            /*
             * API routes
             */
            $router->get('api/regions', ['as' => 'api.regions.index', 'uses' => 'ApiController@index']);
            $router->get('api/regions/search', 'ApiController@search');
            $router->get('api/regions/{id}', 'ApiController@show');
            // create/update
            $router->post('api/regions/{region}/delete', 'ApiController@destroy');
            $router->post('api/regions/move/{region}', 'ApiController@move');
            $router->post('api/regions/reorder/{region}', 'ApiController@reorder');
            $router->post('api/regions/update/{region}/item-state', 'ApiController@setItemState');
            $router->post('api/regions/update/{region}', 'ApiController@update');
            $router->post('api/regions', 'ApiController@store');
            // other routes
            $router->put('api/regions/{region}', ['as' => 'api.regions.update', 'uses' => 'ApiController@update']);
            $router->delete('api/regions/{region}', ['as' => 'api.regions.destroy', 'uses' => 'ApiController@destroy']);
        });
    }
}
