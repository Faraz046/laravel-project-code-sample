<?php

namespace TypiCMS\Modules\Regions\Http\Controllers;

use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Regions\Repositories\RegionInterface;

class PublicController extends BasePublicController
{
    public function __construct(RegionInterface $region)
    {
        parent::__construct($region);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all();

        return view('regions::public.index')
            ->with(compact('models'));
    }

    /**
     * Show news.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $model = $this->repository->bySlug($slug);

        return view('regions::public.show')
            ->with(compact('model'));
    }
}
