<?php

namespace TypiCMS\Modules\Regions\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Request;
use TypiCMS\Modules\Core\Http\Controllers\BaseApiController;
use TypiCMS\Modules\Regions\Models\Region;
use TypiCMS\Modules\Regions\Models\RegionTranslation;
use TypiCMS\Modules\Regions\Repositories\RegionInterface as Repository;

class ApiController extends BaseApiController
{

    /**
     *  Array of endpoints that do not require authorization
     */
    protected $publicEndpoints = [];

    /**
     * ApiController constructor.
     * @param \TypiCMS\Modules\Regions\Repositories\RegionInterface $repository
     */
    public function __construct(Repository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * List resources
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if(Request::get('id') && Request::get('type')) {
            return Region::index()->where('parent_id', Request::get('id'))->get();
        }

        return Region::index()->whereNull('parent_id')->from((new Region)->getTable())->get();
    }

    /**
     * Update the specified resource in storage.
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $region = Region::index()->whereId($id)->first();
        $region->parents = $region->getParents();

        return response()->json(['item' => $region], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        $model = Region::create([
            'parent_id' => Request::get('parent_id') ?: null,
            'category_id' => Request::get('parent_category_id') ?: 1,
            'position' => $this->getMaxPosition() + 1
        ]);

        $model->translation()->save(new RegionTranslation([
            'name' => Request::get('name'),
            'locale' => 'sk',
            'language_id' => 1,
            'escaped_name' => Request::get('name'),
            'description' => '',
            'flight_duration' => '',
            'seo_title' => '',
            'seo_url' => '',
            'seo_description' => '',
            'seo_keywords' => ''
        ]));

        return response()->json(['item' => $model]);
    }

    /**
     * Calculate max position column within records with same parent_id
     * @return int
     */
    private function getMaxPosition()
    {
        if(Request::get('parent_id'))
            return Region::where('parent_id', Request::get('parent_id'))->max('position');

        return Region::whereNull('parent_id')->max('position');
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
        if($record = RegionTranslation::whereItemId($request->get('item_id'))->first()) {
            $record->name = $request->get('name', $record->name);
            $record->description = $request->get('description', $record->description);
            $record->flight_duration = $request->get('flight_duration', $record->flight_duration);
            $record->seo_title = $request->get('meta_title');
            $record->seo_description = $request->get('meta_description');
            $record->seo_keywords = $request->get('meta_keywords');
            $record->seo_url = $request->get('meta_url');

            return response()->json($record->save());
        }

        $record = RegionTranslation::create([
            'name' => $request->get('name'),
            'escaped_name' => $request->get('name'),
            'item_id' => $request->get('item_id')
        ]);

        return response()->json($record->exists ? 1 : 0);
    }

    /**
     * Change item's parent
     * @param \Illuminate\Http\Request $request
     * @param \TypiCMS\Modules\Regions\Models\Region $region
     * @return \Illuminate\Http\JsonResponse
     */
    public function move(\Illuminate\Http\Request $request, Region $region)
    {
        DB::table((new Region)->getTable())
            ->where('parent_id', $request->get('parent_id'))
            ->where('position', '>=', $request->get('position'))
            ->update(['position' => DB::raw('position + 1')]);

        $parent = Region::whereId($request->get('parent_id'))->first();

        if($parent->has_childs == false) {
            $parent->has_childs = true;
            $parent->save();
        }

        $region->position = $request->get('position');
        $region->parent_id = $request->get('parent_id');
        $region->save();

        return response()->json(['item' => $region]);
    }

    /**
     * Reorder item
     * @param \Illuminate\Http\Request $request
     * @param \TypiCMS\Modules\Regions\Models\Region $region
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(\Illuminate\Http\Request $request, Region $region)
    {
        $query = DB::table((new Region)->getTable())->where('parent_id', '=', $region->parent_id);
        $newPosition = $request->get('position');

        if($newPosition > $region->position) {
            $query->whereBetween('position', [$region->position, $newPosition]);
            $query->where('position', '>', 0);
            $query->update(['position' => DB::raw('position - 1')]);
        } else {
            $query->whereBetween('position', [$newPosition, $region->position]);
            $query->update(['position' => DB::raw('position + 1')]);
        }

        $region->position = $newPosition;
        $region->save();

        return response()->json(['item' => $region]);
    }

    /**
     * Update item state request
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return mixed
     */
    public function setItemState(\Illuminate\Http\Request $request, $id)
    {
        $region = Region::findOrFail($id);
        $region->active = (int) $request->get('state');

        return response()->json((int)$region->save());
    }

    /**
     * Remove the specified resource from storage.
     * @param \TypiCMS\Modules\Regions\Models\Region $region
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Region $region)
    {
        $region->delete();
        DB::table((new RegionTranslation)->getTable())->where('item_id', '=', $region->id)->delete();

        return response()->json(1);
    }

    /**
     * Search query
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(\Illuminate\Http\Request $request)
    {
        $keyword = $request->get('query');

        $models = Region::search($keyword)->take(12)->get();

        return response()->json(['results' => $models]);
    }
}
