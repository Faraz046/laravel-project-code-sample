<?php

namespace TypiCMS\Modules\Regions\Http\Controllers;

use Javascript;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Regions\Http\Requests\FormRequest;
use TypiCMS\Modules\Regions\Models\Region;
use TypiCMS\Modules\Regions\Repositories\RegionInterface;

class AdminController extends BaseAdminController
{
    public function __construct(RegionInterface $region)
    {
        parent::__construct($region);
    }

    /**
     * Create form for a new resource.
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $module = 'regions';
        $title = trans($module.'::global.name');
        $models = [];
        JavaScript::put('models', $models);

        return view('core::admin.index')
            ->with(compact('title', 'module', 'models'));
    }

    public function create()
    {
        $model = $this->repository->getModel();

        return view('core::admin.create')
            ->with(compact('model'));
    }

    /**
     * Edit form for the specified resource.
     *
     * @param \TypiCMS\Modules\Regions\Models\Region $region
     *
     * @return \Illuminate\View\View
     */
    public function edit(Region $region)
    {
        return view('core::admin.edit')
            ->with(['model' => $region]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \TypiCMS\Modules\Regions\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $region = $this->repository->create($request->all());

        return $this->redirect($request, $region);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \TypiCMS\Modules\Regions\Models\Region            $region
     * @param \TypiCMS\Modules\Regions\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Region $region, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $region);
    }
}
