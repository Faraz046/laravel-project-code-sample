<?php

namespace TypiCMS\Modules\Regions\Composers;

use Illuminate\Contracts\View\View;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;
use TypiCMS\Modules\Core\Composers\BaseSidebarViewComposer;

class SidebarViewComposer extends BaseSidebarViewComposer
{
    public function compose(View $view)
    {
        $view->sidebar->group(trans('global.menus.content'), function (SidebarGroup $group) {
            $group->addItem(trans('regions::global.name'), function (SidebarItem $item) {
                $item->icon = config('typicms.regions.sidebar.icon');
                $item->weight = config('typicms.regions.sidebar.weight');
                $item->route('admin.regions.index');
                $item->append('admin.regions.create');
                $item->authorize(
                    $this->auth->hasAccess('regions.index')
                );
            });
        });
    }
}
