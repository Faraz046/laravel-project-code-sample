<?php

return [
    'name'         => 'Články',
    'news'         => 'článok|články',
    'New'          => 'Nový článok',
    'Edit'         => 'Editovať článok',
    'Back'         => 'Späť na články',
    'Published on' => 'Zverejnené dňa',
];
