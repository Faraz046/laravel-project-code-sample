<?php

return [
    'name'  => 'Stránky',
    'pages' => 'stránka|stránky',
    'New'   => 'Nová stránka',
    'Edit'  => 'Editovať stránku',
    'Back'  => 'Späť na stránky',
];
