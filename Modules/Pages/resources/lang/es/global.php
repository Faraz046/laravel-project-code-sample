<?php

return [
    'name'                                              => 'Páginas',
    'pages'                                             => 'pagina|paginas',
    'New'                                               => 'Nueva página',
    'Edit'                                              => 'Editar página',
    'Back'                                              => 'Volver a las páginas',
    'A page with children cannot be linked to a module' => 'Una página con descendiente no puede estar vinculada a un módulo.',
];
