<?php

namespace TypiCMS\Modules\Airports\Http\Controllers;

use Auth;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Airports\Http\Requests\FormRequest;
use TypiCMS\Modules\Airports\Models\Airport;
use TypiCMS\Modules\Airports\Repositories\AirportInterface;

class AdminController extends BaseAdminController
{
    /**
     * AdminController constructor.
     * @param \TypiCMS\Modules\Airports\Repositories\AirportInterface $airport
     */
    public function __construct(AirportInterface $airport)
    {
        parent::__construct($airport);
    }

    /**
     * Create form for a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $model = $this->repository->getModel();
        $positions = $this->generatePositionsArray(object_get(Auth::user(), 'company_id'), true);

        return view('core::admin.create')
            ->with(compact('model', 'positions'));
    }

    /**
     * Edit form for the specified resource.
     *
     * @param \TypiCMS\Modules\Airports\Models\Airport $airport
     *
     * @return \Illuminate\View\View
     */
    public function edit(Airport $airport)
    {
        return view('core::admin.edit')
            ->with([
                'model' => $airport,
                'positions' => $this->generatePositionsArray($airport->company_id)
            ]);
    }

    /**
     * Generate positions array
     * @param $companyId
     * @param bool $new
     * @return array
     */
    private function generatePositionsArray($companyId, $new = false)
    {
        if($companyId) {
            $max = Airport::where('company_id', $companyId)->max('position') + 1;
        } else {
            $max = Airport::whereNull('company_id')->max('position') + 1;
        }

        if($new === true) {
            $max += 1;
        }

        $data = [];

        for($i = 1; $i < $max; ++$i) {
            $data[$i] = $i;
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \TypiCMS\Modules\Airports\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $airport = $this->repository->create($request->all());

        return $this->redirect($request, $airport);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \TypiCMS\Modules\Airports\Models\Airport            $airport
     * @param \TypiCMS\Modules\Airports\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Airport $airport, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $airport);
    }
}
