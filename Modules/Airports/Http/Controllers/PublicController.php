<?php

namespace TypiCMS\Modules\Airports\Http\Controllers;

use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Airports\Repositories\AirportInterface;

class PublicController extends BasePublicController
{
    public function __construct(AirportInterface $airport)
    {
        parent::__construct($airport);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all();

        return view('airports::public.index')
            ->with(compact('models'));
    }

    /**
     * Show news.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $model = $this->repository->bySlug($slug);

        return view('airports::public.show')
            ->with(compact('model'));
    }
}
