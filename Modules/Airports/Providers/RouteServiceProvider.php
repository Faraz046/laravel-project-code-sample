<?php

namespace TypiCMS\Modules\Airports\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use TypiCMS\Modules\Core\Facades\TypiCMS;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TypiCMS\Modules\Airports\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {

            /*
             * Front office routes
             */
            if ($page = TypiCMS::getPageLinkedToModule('airports')) {
                $options = $page->private ? ['middleware' => 'auth'] : [];
                foreach (config('translatable.locales') as $lang) {
                    if ($page->translate($lang)->status && $uri = $page->uri($lang)) {
                        $router->get($uri, $options + ['as' => $lang.'.airports', 'uses' => 'PublicController@index']);
                        $router->get($uri.'/{slug}', $options + ['as' => $lang.'.airports.slug', 'uses' => 'PublicController@show']);
                    }
                }
            }

            /*
             * Admin routes
             */
            $router->get('admin/airports', ['as' => 'admin.airports.index', 'uses' => 'AdminController@index']);
            $router->get('admin/airports/create', ['as' => 'admin.airports.create', 'uses' => 'AdminController@create']);
            $router->get('admin/airports/{airport}/edit', ['as' => 'admin.airports.edit', 'uses' => 'AdminController@edit']);
            $router->post('admin/airports', ['as' => 'admin.airports.store', 'uses' => 'AdminController@store']);
            $router->put('admin/airports/{airport}', ['as' => 'admin.airports.update', 'uses' => 'AdminController@update']);

            /*
             * API routes
             */
            $router->get('api/airports', ['as' => 'api.airports.index', 'uses' => 'ApiController@index']);
            $router->put('api/airports/{airport}', ['as' => 'api.airports.update', 'uses' => 'ApiController@update']);
            $router->delete('api/airports/{airport}', ['as' => 'api.airports.destroy', 'uses' => 'ApiController@destroy']);
        });
    }
}
