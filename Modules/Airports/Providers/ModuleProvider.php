<?php

namespace TypiCMS\Modules\Airports\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\FileObserver;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Core\Services\Cache\LaravelCache;
use TypiCMS\Modules\Airports\Models\Airport;
use TypiCMS\Modules\Airports\Models\AirportTranslation;
use TypiCMS\Modules\Airports\Repositories\CacheDecorator;
use TypiCMS\Modules\Airports\Repositories\EloquentAirport;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'typicms.airports'
        );

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['airports' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'airports');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'airports');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/airports'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../database' => base_path('database'),
        ], 'migrations');

        AliasLoader::getInstance()->alias(
            'Airports',
            'TypiCMS\Modules\Airports\Facades\Facade'
        );

        // Observers
        AirportTranslation::observe(new SlugObserver());
        Airport::observe(new FileObserver());
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register('TypiCMS\Modules\Airports\Providers\RouteServiceProvider');

        /*
         * Sidebar view composer
         */
        $app->view->composer('core::admin._sidebar', 'TypiCMS\Modules\Airports\Composers\SidebarViewComposer');

        /*
         * Add the page in the view.
         */
        $app->view->composer('airports::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('airports');
        });

        $app->bind('TypiCMS\Modules\Airports\Repositories\AirportInterface', function (Application $app) {
            $repository = new EloquentAirport(new Airport());
            if (!config('typicms.cache')) {
                return $repository;
            }
            $laravelCache = new LaravelCache($app['cache'], 'airports', 10);

            return new CacheDecorator($repository, $laravelCache);
        });
    }
}
