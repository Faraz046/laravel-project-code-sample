<?php

namespace TypiCMS\Modules\Airports\Composers;

use Illuminate\Contracts\View\View;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;
use TypiCMS\Modules\Core\Composers\BaseSidebarViewComposer;

class SidebarViewComposer extends BaseSidebarViewComposer
{
    public function compose(View $view)
    {
        $view->sidebar->group(trans('global.menus.filters'), function (SidebarGroup $group) {
            $group->addItem(trans('airports::global.name'), function (SidebarItem $item) {
                $item->icon = config('typicms.airports.sidebar.icon');
                $item->weight = config('typicms.airports.sidebar.weight');
                $item->route('admin.airports.index');
                $item->append('admin.airports.create');
                $item->authorize(
                    $this->auth->hasAccess('airports.index')
                );
            });
        });
    }
}
