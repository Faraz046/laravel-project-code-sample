<?php

return [
    'per_page' => 30,
    'order' => [
        'position' => 'asc',
    ],
    'sidebar' => [
        'icon' => 'icon fa fa-fw fa-cube',
        'weight' => 8,
    ],
];
