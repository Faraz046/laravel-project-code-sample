<?php

namespace TypiCMS\Modules\Airports\Repositories;

use TypiCMS\Modules\Core\Repositories\RepositoryInterface;

interface AirportInterface extends RepositoryInterface
{
}
