<?php

namespace TypiCMS\Modules\Airports\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;

class EloquentAirport extends RepositoriesAbstract implements AirportInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
}
