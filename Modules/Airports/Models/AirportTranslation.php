<?php

namespace TypiCMS\Modules\Airports\Models;

use Auth;
use TypiCMS\Modules\Core\Models\BaseTranslation;

class AirportTranslation extends BaseTranslation
{
    /**
     * Booting model
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            if(is_null($model->company_id) && (bool)Auth::user()->owner === false && ! is_null(Auth::user()->company_id)) {
                $model->company_id = Auth::user()->company_id;
            }
        });
    }

    /**
     * get the parent model.
     */
    public function owner()
    {
        return $this->belongsTo('TypiCMS\Modules\Airports\Models\Airport', 'airport_id');
    }

    /**
     * Mock firing model event
     * @param string $event
     * @param bool $halt
     * @return null
     */
    public function fireModelEvent($event, $halt = true)
    {
        //
    }
}
