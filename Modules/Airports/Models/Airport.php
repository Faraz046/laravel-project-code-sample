<?php

namespace TypiCMS\Modules\Airports\Models;

use Auth;
use DB;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Laracasts\Presenter\PresentableTrait;
use TypiCMS\Modules\Companies\Models\Company;
use TypiCMS\Modules\Core\Models\Base;
use TypiCMS\Modules\History\Traits\Historable;

class Airport extends Base
{
    use Historable;
    use PresentableTrait;
    use Translatable;

    /**
     * Presenter
     * @var string
     */
    protected $presenter = 'TypiCMS\Modules\Airports\Presenters\ModulePresenter';

    protected $with = ['company'];
    
    /**
     * Declare any properties that should be hidden from JSON Serialization.
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'image',
        'code',
        'active',
        'company_id',
        'position',
        // Translatable columns
        'title',
        'slug',
        'status',
    ];

    /**
     * Translatable model configs.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title',
        'slug',
        'status',
    ];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['status', 'title', 'thumb'];

    /**
     * Columns that are file.
     * @var array
     */
    public $attachments = [
        'image',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            if(is_null($model->company_id) && (bool)Auth::user()->owner === false && ! is_null(Auth::user()->company_id)) {
                $model->company_id = Auth::user()->company_id;
            }

            $model->position = Airport::where('company_id', $model->company_id)->max('position') + 1;
        });

        static::saving(function(Airport $model) {
            if((int)$model->position !== (int)$model->getOriginal('position')) {
                $newPosition = (int)$model->position;
                $oldPosition = (int)$model->getOriginal('position');

                if($newPosition > $oldPosition) {
                    Airport::where('company_id', $model->company_id)
                        ->where('position', '>', $oldPosition)
                        ->where('position', '<=', $newPosition)
                        ->where('position', '>', 0)
                        ->update(['position' => DB::raw('position - 1')]);
                } else {
                    Airport::where('company_id', $model->company_id)
                        ->where('position', '<', $oldPosition)
                        ->where('position', '>=', $newPosition)
                        ->update(['position' => DB::raw('position + 1')]);
                }
            }
        });

        static::addGlobalScope('company', function($query) {
            if(! Auth::check()) {
                return $query;
            }

            if((bool)Auth::user()->owner === true) {
                return $query;
            }

            if((bool)Auth::user()->owner === false && ! is_null(Auth::user()->company_id)) {
                return $query->where('company_id', Auth::user()->company_id);
            }
        });
    }

    /**
     * Company relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
