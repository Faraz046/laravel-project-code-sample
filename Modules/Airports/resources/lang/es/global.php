<?php

return [
    'name'     => 'Objetos',
    'airports'  => 'objeto|objetos',
    'New'      => 'Nuevo objeto',
    'Edit'     => 'Editar objeto',
    'Back'     => 'Volver a los objetos',
];
