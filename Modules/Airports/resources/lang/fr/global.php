<?php

return [
    'name'     => 'Airports',
    'airports'  => 'airport|airports',
    'New'      => 'Nouveau airport',
    'Edit'     => 'Modifier airport',
    'Back'     => 'Retour à la liste des airports',
];
