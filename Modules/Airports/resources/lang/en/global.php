<?php

return [
    'name'     => 'Airports',
    'airports'  => 'airport|airports',
    'New'      => 'New airport',
    'Edit'     => 'Edit airport',
    'Back'     => 'Back to airports',
];
