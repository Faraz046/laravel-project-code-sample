<?php

return [
    'name'         => 'Preklady',
    'translations' => 'preklad|prekladov',
    'New'          => 'Nový preklad',
    'Edit'         => 'Editovať preklad',
    'Back'         => 'Späť na preklady',
];
