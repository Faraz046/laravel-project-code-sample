<?php

namespace TypiCMS\Modules\Slides\Repositories;

use Auth;
use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;

class EloquentSlide extends RepositoriesAbstract implements SlideInterface
{
    /**
     * EloquentSlide constructor.
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all models.
     *
     * @param array $with Eager load related models
     * @param bool  $all  Show published or all
     *
     * @return Collection|NestedCollection
     */
    public function all(array $with = [], $all = false)
    {
        $query = $this->make($with);

        if (!$all) {
            $query->online();
        }

        if(Auth::check() && Auth::user()->owner === false) {
            $query->whereNotNull('company_id');

            if(! is_null(Auth::user()->company_id)) {
                $query->where('company_id', Auth::user()->company_id);
            }
        }

        // Query ORDER BY
        $query->order();

        // Get
        return $query->get();
    }
}
