<?php

return [
    'name'     => 'Slider',
    'slides'   => 'slide|slidy',
    'New'      => 'Nový slide',
    'Edit'     => 'Editovať slide',
    'Back'     => 'Späť na slidy',
];
