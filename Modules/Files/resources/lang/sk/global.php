﻿<?php

return [
    'name'                          => 'Súbory',
    'files'                         => 'súbor|súbory',
    'New'                           => 'Nový súbor',
    'Edit'                          => 'Editovať súbor',
    'Back'                          => 'Späť na súbory',
    'Click or drop files to upload' => 'Kliknite alebo pretiahnite sem.',
    'Drop files to upload'          => 'Pretiahnite pre nahratie.',
    'Insert'                        => 'Vložiť',
];
