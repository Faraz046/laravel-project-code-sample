<?php

namespace TypiCMS\Modules\Companies\Models;

use Laracasts\Presenter\PresentableTrait;
use TypiCMS\Modules\Airports\Models\Airport;
use TypiCMS\Modules\Core\Models\Base;
use TypiCMS\Modules\Users\Models\User;

class Company extends Base
{
    use PresentableTrait;

    protected $presenter = 'TypiCMS\Modules\Companies\Presenters\ModulePresenter';
    
    /**
     * Declare any properties that should be hidden from JSON Serialization.
     *
     * @var array
     */
    protected $hidden = [];

    protected $fillable = [
        'name',
    ];

    /**
     * Translatable model configs.
     *
     * @var array
     */
    public $translatedAttributes = [

    ];

    protected $appends = [];

    /**
     * Columns that are file.
     *
     * @var array
     */
    public $attachments = [

    ];

    public static function boot()
    {
        parent::boot();

        static::created(function(Company $company) {
            //
        });

        static::deleting(function(Company $company) {
            Airport::withoutGlobalScopes()->where('company_id', $company->id)->delete();
        });
    }

    public function setUserIdAttribute($value)
    {
        $this->attributes['user_id'] = (! is_null($value) && $value !== 0) ? $value : null;
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
