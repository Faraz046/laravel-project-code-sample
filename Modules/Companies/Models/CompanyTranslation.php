<?php

namespace TypiCMS\Modules\Companies\Models;

use TypiCMS\Modules\Core\Models\BaseTranslation;

class CompanyTranslation extends BaseTranslation
{
    /**
     * get the parent model.
     */
    public function owner()
    {
        return $this->belongsTo('TypiCMS\Modules\Companies\Models\Company', 'company_id');
    }
}
