<?php

namespace TypiCMS\Modules\Companies\Repositories;

use TypiCMS\Modules\Core\Repositories\RepositoryInterface;

interface CompanyInterface extends RepositoryInterface
{
}
