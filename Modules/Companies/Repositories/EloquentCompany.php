<?php

namespace TypiCMS\Modules\Companies\Repositories;

use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;
use TypiCMS\Modules\Users\Models\User;

class EloquentCompany extends RepositoriesAbstract implements CompanyInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Create a new model.
     *
     * @param array $data
     *
     * @return mixed Model or false on error during save
     */
    public function create(array $data)
    {
        // Create the model
        $model = $this->model->fill($data);

        if ($model->save()) {
            $this->syncRelation($model, $data, 'galleries');

            if(! empty($data['users'])) {
                User::whereIn('id', array_get($data, 'users'))->update(['company_id' => $model->id]);
            }

            return $model;
        }

        return false;
    }

    /**
     * Update an existing model.
     *
     * @param array $data
     *
     * @return bool
     */
    public function update(array $data)
    {
        $model = $this->model->find($data['id']);

        $model->fill($data);

        $this->syncRelation($model, $data, 'galleries');

        if ($model->save()) {
            // Refresh company_id columns
            User::where('company_id', $model->id)->update(['company_id' => null]);

            if(! empty($data['users'])) {
                User::whereIn('id', array_get($data, 'users'))->update(['company_id' => $model->id]);
            }

            return true;
        }

        return false;
    }
}
