<?php

namespace TypiCMS\Modules\Companies\Http\Controllers;

use TypiCMS\Modules\Core\Http\Controllers\BasePublicController;
use TypiCMS\Modules\Companies\Repositories\CompanyInterface;

class PublicController extends BasePublicController
{
    public function __construct(CompanyInterface $company)
    {
        parent::__construct($company);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $models = $this->repository->all();

        return view('companies::public.index')
            ->with(compact('models'));
    }

    /**
     * Show news.
     *
     * @return \Illuminate\View\View
     */
    public function show($slug)
    {
        $model = $this->repository->bySlug($slug);

        return view('companies::public.show')
            ->with(compact('model'));
    }
}
