<?php

namespace TypiCMS\Modules\Companies\Http\Controllers;

use App\Jobs\GenerateAirports;
use App\Jobs\GenerateTouroperators;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use TypiCMS\Modules\Core\Http\Controllers\BaseAdminController;
use TypiCMS\Modules\Companies\Http\Requests\FormRequest;
use TypiCMS\Modules\Companies\Models\Company;
use TypiCMS\Modules\Companies\Repositories\CompanyInterface;
use TypiCMS\Modules\Users\Models\User;

class AdminController extends BaseAdminController
{
    use AuthorizesRequests, DispatchesJobs;

    /**
     * AdminController constructor.
     * @param \TypiCMS\Modules\Companies\Repositories\CompanyInterface $company
     */
    public function __construct(CompanyInterface $company)
    {
        parent::__construct($company);
    }

    /**
     * Companies page
     * @return \Illuminate\View\View|\Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Company::class);

        return parent::index();
    }

    /**
     * Create form for a new resource.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', Company::class);

        $model = $this->repository->getModel();
        $users = User::all();

        return view('core::admin.create')
            ->with(compact('model', 'users'));
    }

    /**
     * Edit form for the specified resource.
     *
     * @param \TypiCMS\Modules\Companies\Models\Company $company
     *
     * @return \Illuminate\View\View
     */
    public function edit(Company $company)
    {
        $this->authorize('edit', Company::class);

        return view('core::admin.edit')
            ->with([
                'model' => $company,
                'users' => User::all()
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \TypiCMS\Modules\Companies\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FormRequest $request)
    {
        $this->authorize('store', Company::class);

        $company = $this->repository->create($request->all());

        $this->dispatch(new GenerateAirports($company));

        $this->dispatch(new GenerateTouroperators($company));

        return $this->redirect($request, $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \TypiCMS\Modules\Companies\Models\Company            $company
     * @param \TypiCMS\Modules\Companies\Http\Requests\FormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Company $company, FormRequest $request)
    {
        $this->authorize('update', $company);

        $this->repository->update($request->all());

        return $this->redirect($request, $company);
    }
}
