<?php

namespace TypiCMS\Modules\Companies\Http\Requests;

use TypiCMS\Modules\Core\Http\Requests\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'name' => 'unique:companies,id,' . $this->get('id'),
        ];
    }
}
