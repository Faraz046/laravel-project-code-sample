<?php

namespace TypiCMS\Modules\Companies\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Core\Facades\TypiCMS;
use TypiCMS\Modules\Core\Observers\FileObserver;
use TypiCMS\Modules\Core\Observers\SlugObserver;
use TypiCMS\Modules\Core\Services\Cache\LaravelCache;
use TypiCMS\Modules\Companies\Models\Company;
use TypiCMS\Modules\Companies\Models\CompanyTranslation;
use TypiCMS\Modules\Companies\Repositories\CacheDecorator;
use TypiCMS\Modules\Companies\Repositories\EloquentCompany;

class ModuleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'typicms.companies'
        );

        $modules = $this->app['config']['typicms']['modules'];
        $this->app['config']->set('typicms.modules', array_merge(['companies' => ['linkable_to_page']], $modules));

        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'companies');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'companies');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/companies'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../database' => base_path('database'),
        ], 'migrations');

        AliasLoader::getInstance()->alias(
            'Companies',
            'TypiCMS\Modules\Companies\Facades\Facade'
        );

        // Observers
        CompanyTranslation::observe(new SlugObserver());
        Company::observe(new FileObserver());
    }

    public function register()
    {
        $app = $this->app;

        /*
         * Register route service provider
         */
        $app->register('TypiCMS\Modules\Companies\Providers\RouteServiceProvider');

        /*
         * Sidebar view composer
         */
        $app->view->composer('core::admin._sidebar', 'TypiCMS\Modules\Companies\Composers\SidebarViewComposer');

        /*
         * Add the page in the view.
         */
        $app->view->composer('companies::public.*', function ($view) {
            $view->page = TypiCMS::getPageLinkedToModule('companies');
        });

        $app->bind('TypiCMS\Modules\Companies\Repositories\CompanyInterface', function (Application $app) {
            $repository = new EloquentCompany(new Company());
            if (!config('typicms.cache')) {
                return $repository;
            }
            $laravelCache = new LaravelCache($app['cache'], 'companies', 10);

            return new CacheDecorator($repository, $laravelCache);
        });
    }
}
