<?php

namespace TypiCMS\Modules\Companies\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use TypiCMS\Modules\Core\Facades\TypiCMS;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'TypiCMS\Modules\Companies\Http\Controllers';

    /**
     * Define the routes for the application.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function (Router $router) {

            /*
             * Front office routes
             */
            if ($page = TypiCMS::getPageLinkedToModule('companies')) {
                $options = $page->private ? ['middleware' => 'auth'] : [];
                foreach (config('translatable.locales') as $lang) {
                    if ($page->translate($lang)->status && $uri = $page->uri($lang)) {
                        $router->get($uri, $options + ['as' => $lang.'.companies', 'uses' => 'PublicController@index']);
                        $router->get($uri.'/{slug}', $options + ['as' => $lang.'.companies.slug', 'uses' => 'PublicController@show']);
                    }
                }
            }

            /*
             * Admin routes
             */
            $router->get('admin/companies', ['as' => 'admin.companies.index', 'uses' => 'AdminController@index']);
            $router->get('admin/companies/create', ['as' => 'admin.companies.create', 'uses' => 'AdminController@create']);
            $router->get('admin/companies/{company}/edit', ['as' => 'admin.companies.edit', 'uses' => 'AdminController@edit']);
            $router->post('admin/companies', ['as' => 'admin.companies.store', 'uses' => 'AdminController@store']);
            $router->put('admin/companies/{company}', ['as' => 'admin.companies.update', 'uses' => 'AdminController@update']);

            /*
             * API routes
             */
            $router->get('api/companies', ['as' => 'api.companies.index', 'uses' => 'ApiController@index']);
            $router->put('api/companies/{company}', ['as' => 'api.companies.update', 'uses' => 'ApiController@update']);
            $router->delete('api/companies/{company}', ['as' => 'api.companies.destroy', 'uses' => 'ApiController@destroy']);
        });
    }
}
