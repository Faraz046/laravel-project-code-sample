<?php

return [
    'name'     => 'Companies',
    'companies'  => 'company|companies',
    'New'      => 'Nouveau company',
    'Edit'     => 'Modifier company',
    'Back'     => 'Retour à la liste des companies',
];
