<?php

return [
    'name'     => 'Companies',
    'companies'  => 'company|companies',
    'New'      => 'New company',
    'Edit'     => 'Edit company',
    'Back'     => 'Back to companies',
];
