<?php

return [
    'name'     => 'Cestovné agentúry',
    'companies'  => 'company|companies',
    'New'      => 'New company',
    'Edit'     => 'Edit company',
    'Back'     => 'Back to companies',
];
