<?php

namespace TypiCMS\Modules\Companies\Presenters;

use TypiCMS\Modules\Core\Presenters\Presenter;

class ModulePresenter extends Presenter
{
    public function title()
    {
        return $this->name;
    }
}
