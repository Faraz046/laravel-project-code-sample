<?php

namespace TypiCMS\Modules\Companies\Composers;

use Illuminate\Contracts\View\View;
use Maatwebsite\Sidebar\SidebarGroup;
use Maatwebsite\Sidebar\SidebarItem;
use TypiCMS\Modules\Core\Composers\BaseSidebarViewComposer;
use TypiCMS\Modules\Companies\Models\Company;
use Gate;

class SidebarViewComposer extends BaseSidebarViewComposer
{
    public function compose(View $view)
    {
        if (Gate::allows('create', Company::class)) {
            $view->sidebar->group(trans('global.menus.content'), function (SidebarGroup $group) {
                $group->addItem(trans('companies::global.name'), function (SidebarItem $item) {
                    $item->icon = config('typicms.companies.sidebar.icon');
                    $item->weight = config('typicms.companies.sidebar.weight');
                    $item->route('admin.companies.index');
                    $item->append('admin.companies.create');
                    $item->authorize(
                        $this->auth->hasAccess('companies.index')
                    );
                });
            });
        }
    }
}
