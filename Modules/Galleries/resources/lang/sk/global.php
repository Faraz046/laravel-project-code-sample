﻿<?php

return [
    'name'                               => 'Galérie',
    'galleries'                          => 'galéria|galérie',
    'New'                                => 'Nová galéria',
    'Edit'                               => 'Editovať galériu',
    'Back'                               => 'Späť na galérie',
    'Save your gallery, then add files.' => 'Uložte galériu, potom pridajte súbory.',
];
