<?php

namespace TypiCMS\Modules\Galleries\Repositories;

use Auth;
use Illuminate\Database\Eloquent\Model;
use TypiCMS\Modules\Core\Repositories\RepositoriesAbstract;
use TypiCMS\Modules\Files\Models\File;

class EloquentGallery extends RepositoriesAbstract implements GalleryInterface
{
    /**
     * EloquentGallery constructor.
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all models.
     *
     * @param array $with Eager load related models
     * @param bool  $all  Show published or all
     *
     * @return Collection|NestedCollection
     */
    public function all(array $with = [], $all = false)
    {
        $query = $this->make($with);

        if (!$all) {
            $query->online();
        }

        if(Auth::check() && Auth::user()->owner === false) {
            $query->whereNotNull('company_id');

            if(! is_null(Auth::user()->company_id)) {
                $query->where('company_id', Auth::user()->company_id);
            }
        }

        // Query ORDER BY
        $query->order();

        // Get
        return $query->get();
    }

    /**
     * Delete model and attached files.
     *
     * @return bool
     */
    public function delete($model)
    {
        if ($model->files) {
            $model->files->each(function (File $file) {
                $file->delete();
            });
        }
        if ($model->delete()) {
            return true;
        }

        return false;
    }
}
