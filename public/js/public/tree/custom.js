var oldMovedNode;
var lang = {};

lang.alert_change_state = "Ľutujeme, ale vyskytla sa chyba počas zmeny stavu položky!";
lang.alert_copy = "Ľutujeme, ale vyskytla sa chyba počas kopírovania položky!";
lang.alert_create = "Ľutujeme, ale vyskytla sa chyba počas vytvárania položky!";
lang.alert_delete = "Ľutujeme, ale vyskytla sa chyba počas vymazania položky!";
lang.alert_delete_image = "Ľutujeme, ale vyskytla sa chyba počas vymazania obrázka!";
lang.alert_load_item = "Ľutujeme, ale vyskytla sa chyba počas načítavania položky!";
lang.alert_move = "Ľutujeme, ale vyskytla sa chyba počas presúvania položky!";
lang.alert_rename = "Ľutujeme, ale vyskytla sa chyba počas premenovania položky!";
lang.alert_reorder = "Ľutujeme, ale vyskytla sa chyba počas zmeny poradia!";
lang.alert_reorder_image = "Ľutujeme, ale vyskytla sa chyba počas zmeny poradia obrázka!";
lang.alert_save = "Ľutujeme, ale vyskytla sa chyba počas ukladania zmien!";
lang.confirm_delete_image = "Naozaj chcete vymazať obrázok?";
lang.confirm_delete_item = "Naozaj chcete vymazať položku ?";
lang.confirm_reorder = "Naozaj chcete presunúť <br>z %d. pozície na %d. pozíciu?";
lang.delete_image_title = "Vymazať obrázok";
lang.new_node = "Nové";
lang.no_results = "Žiadne výsledky";
lang.region_loading = "Načítavanie regiónov...";
lang.title_activate = "Aktivovať alebo deaktivovať";
lang.title_create_new = "Vytvoriť nové";
lang.title_delete = "Vymazať";
lang.title_edit = "Upraviť";

$(document).ready(function(){
	var clickTimer, movedNode, syncAction, selectedResultNode;

	var jstreeHandler = $('[data-jstree="true"]').jstree({
		'plugins' : [ 'checkbox', 'dnd', 'changed' ],
		// zrusime o(d)znacenie kliknutim na cely element (treba kliknut priamo na checkbox)
		'checkbox': {
			'whole_node': false,
			'tie_selection': false
		},
		'core' : {
			// dvojitym kliknutim chceme editovat nazov
			'dblclick_toggle': false,
			// zakazanie animacie
			'animation': false,
			// presuvanie (drag and drop)
			'check_callback': function(operation, node, node_parent, node_position, more){

				if (operation == 'move_node' && node.li_attr) {

					// regiony
					if (node.li_attr['data-category-type'] == 'region') {
						// presuvanie medzi regionmi (zmena poradia)
						if (node_parent.id && node_parent.id == '#') {
							return true;
						}
					}

					// destinacie
					if (node.li_attr['data-category-type'] == 'destination') {
						// pod regiony (a zaroven zmena poradia)
						if (node_parent.li_attr && node_parent.li_attr['data-category-type'] == 'region') {
							return true;
						}
					}

					// mesta
					if (node.li_attr['data-category-type'] == 'city' && node.parent == node_parent.id) {
						// presuvanie medzi mestami (zmena poradia)
						if (node_parent.li_attr && node_parent.li_attr['data-category-type'] == 'destination') {
							return true;
						}
					}

					// hotely
					if (node.li_attr['data-category-type'] == 'hotel' && node.parent == node_parent.id) {
						// presuvanie medzi mestami (zmena poradia)
						if (node_parent.li_attr && node_parent.li_attr['data-category-type'] == 'city') {
							return true;
						}
					}

					return false;
				}

				return true;
			},
			// ajaxove nacitavanie
			'data' : {
				'url' : function(node){
					var url = '/api/regions?getItems';

					if (node.li_attr) {
						url += '&type=' + node.li_attr['data-category-type'];
					}

					return url;
				},
				'data' : function(node){
					return { 'id' : node.id && node.id != '#' ? node.id : null };
				}
			},
			'strings': {
				'Loading ...': lang.region_loading,
				'New node': lang.new_node
			}
		},
		'dnd': {
			// cas, kym sa otvori polozka pri presuvani
			'open_timeout': 1000
		}
	})
	// po kliknuti otvorime childy
	.on('select_node.jstree', function(e, data){
		// kliknutie pravym tlacidlom neriesime
		if (data.event && data.event.which && data.event.which === 3) return;

		if (clickTimer) clearTimeout(clickTimer);

		clickTimer = setTimeout(function(){
			if (clickTimer) {
				data.instance.toggle_node(data.node);
			}
		}, 200);
	})
	// po dvojitom kliknuti editujeme nazov
	.on('dblclick.jstree', function(e){
		clearTimeout(clickTimer);

		var tree = $(e.target).closest('div[data-jstree="true"]'),
			ref = tree.jstree(true),
			sel = ref.get_selected();

		if (!sel.length) { return false; }
		ref.edit(sel[0]);
	})
	// link na (de)aktivovanie
	.on('load_node.jstree after_open.jstree rename_node.jstree delete_node.jstree copy_node.jstree move_node.jstree', function(e, data){
		var el, children;
		setTimeout(function(){
			$(data.instance.element).find('li[role="treeitem"]').each(function(){
				el = $(this);
				if ( el.children('a[href="#changeState"]').length > 0 )
					return;

				children = el.children('a');

				// vymazanie
				if (el.attr('data-custom') == '1') {
					children.after('&nbsp;<a href="#deleteItem" class="itemIcon red" title="' + lang.title_delete + '"><i class="fa fa-times"></i></a>');
				}
				// vytvorenie
				if (el.attr('data-category-type') != 'hotel') {
					children.after('&nbsp;<a href="#createItem" class="itemIcon" title="' + lang.title_create_new + '"><i class="fa fa-plus"></i></a>')
				}

				children
					.after('&nbsp;<a href="#editItem" class="itemIcon" title="' + lang.title_edit + '"><i class="fa fa-pencil"></i></a>')
					.after('&nbsp;<a href="#changeState" class="buttonActive itemIcon ' + (el.attr('data-active') == '1' ? 'active' : '') + '" title="' + lang.title_activate + '"><i class="fa fa-circle"></i></a>');

			})
		}, 0);
	})
	// premenovanie
	.on('rename_node.jstree', function(e, data){
		if (syncAction)
			return syncAction = false;

		$.post('/api/regions/update/' + data.node.id, {
			'name'   : data.text,
			'item_id': data.node.id
		}, function(d){
			if (d && d == '1') {
				syncTree('rename_node', data.instance, data.node);
			}
			else {
				showAlert(lang.alert_rename);
			}
		});
	})
	// vytvorenie
	.on('create_node.jstree', function(e, data){
		if (syncAction)
			return syncAction = false;

		var parentId = data.parent !== '#' ? data.parent : null;
		var parentCategoryId = data.parent !== '#' ? data.instance.get_node(data.parent).li_attr['data-category-id'] : null;

		$.post('/api/regions', {
			'parent_id'         : parentId,
			'parent_category_id': parentCategoryId,
			'name'              : data.node.text
		}, function(d){

			if (d && d.item) {
				data.instance.set_id(data.node, d.item.id);
				data.node.li_attr['data-custom'] = d.item.custom == '1' ? '1' : '0';
				data.node.li_attr['data-active'] = d.item.active == '1' ? '1' : '0';
				data.node.li_attr['data-category-id'] = d.item.category_id;
				data.node.li_attr['data-category-type'] = d.item.category_type;

				syncTree('create_node', data.instance, data.node, data);

				data.instance.edit(d.item.id);
			}
			else {
				showAlert(lang.alert_create);
			}
		}, 'json');
	})
	// kopirovanie a presuvanie
	.on('copy_node.jstree move_node.jstree', function(e, data){
		console.log(e);
		console.log(data);
		console.log(syncAction);
		if (syncAction)
			return syncAction = false;

		movedNode = data;

		// otvorime parenta
		movedNode.instance.open_node(data.parent);

		// zmena poradia
		if (movedNode.old_parent == movedNode.parent) {

			$('#modalConfirmReorderQuestion')
				.html(
					lang.confirm_reorder
						.replace('%s', movedNode.node.text)
						.replace('%d', (parseInt(movedNode.old_position) + 1))
						.replace('%d', (parseInt(movedNode.position) + 1))
				);

			$('#modalConfirmReorder').modal({
				'keyboard': false,
				'backdrop': 'static'
			});

			return;
		}

		$('#modalConfirmDnd').modal({
			'keyboard': false,
			'backdrop': 'static'
		});
	});

	// zmena poradia
	$('#buttonDndReorderSuccess').on('click', function(e){

		var itemId = movedNode.node.id;
		if (oldMovedNode && oldMovedNode.id) {
			itemId = oldMovedNode.id;
		}

		$.post('/api/regions/reorder/' + itemId, {
			'position': movedNode.position,
			'item_id' : itemId
		}, function(d){
			if (d && d.item) {
				syncTree('reorder_node', movedNode.instance, movedNode.node, movedNode);
			}
			else {
				showAlert(lang.alert_reorder);
			}
		}, 'json');

		$('#modalConfirmReorder').modal('hide');
	});
	$('#buttonDndReorderCancel').on('click', function(e){
		// navrat spat
		movedNode.instance.move_node(movedNode.node, movedNode.old_parent, movedNode.old_position);

		$('#modalConfirmReorder').modal('hide');
	});

	// presun polozky
	$('#buttonDndMove').on('click', function(e){

		var itemId = (movedNode.original !== undefined) ? movedNode.original.id : movedNode.node.id;

		if (oldMovedNode && oldMovedNode.id) {
			itemId = oldMovedNode.id;
		}

		$.post('/api/regions/move/' + itemId, {
			'position'  : movedNode.position,
			'parent_id' : movedNode.parent,
			'item_id'   : itemId
		}, function(d){
			if (d && d.item) {

				// original nepremiestnujeme, len vytvorime kopiu
				if (oldMovedNode && oldMovedNode.li_attr['data-custom'] && oldMovedNode.li_attr['data-custom'] == '0') {
					// povodny deaktivujeme
					$('#' + oldMovedNode.id).children('a[href="#changeState"]').removeClass('active');
				} else {
					$('#' + itemId).remove();
					//oldMovedNode.origin.delete_node(oldMovedNode.id);
				}

				movedNode.instance.set_id(movedNode.node, d.item.id);
				movedNode.node.li_attr['data-custom'] = d.item.custom == '1' ? '1' : '0';
				movedNode.node.li_attr['data-active'] = d.item.active == '1' ? '1' : '0';
				movedNode.node.li_attr['data-category-id'] = d.item.category_id;
				movedNode.node.li_attr['data-category-type'] = d.item.category_type;

				movedNode.instance.load_node(d.item.id);

				// todo
				//syncTree('move_node', movedNode.instance, movedNode.node);
			}
			else {
				showAlert(lang.alert_move);
			}
		}, 'json');

		$('#modalConfirmDnd').modal('hide');
	});

	// kopirovanie polozky
	$('#buttonDndCopy').on('click', function(e){

		var newParent = movedNode.parent;
		var newPosition = movedNode.position;
		var itemId = movedNode.node.id;
		if (oldMovedNode && oldMovedNode.id) {
			itemId = oldMovedNode.id;
		}

		$.post('', {
			'action'    : 'copyItem',
			'position'  : newPosition,
			'parent_id' : newParent,
			'item_id'   : itemId
		}, function(d){
			if (d && d.item) {

				movedNode.instance.set_id(movedNode.node, d.item.id);
				movedNode.node.li_attr['data-custom'] = d.item.custom == '1' ? '1' : '0';
				movedNode.node.li_attr['data-active'] = d.item.active == '1' ? '1' : '0';
				movedNode.node.li_attr['data-category-id'] = d.item.category_id;
				movedNode.node.li_attr['data-category-type'] = d.item.category_type;

				movedNode.instance.load_node(d.item.id);

				if (oldMovedNode) {
					// todo
					//syncTree('copy_node', oldMovedNode.origin, oldMovedNode);
				} else {
					// todo
					//syncTree('copy_node', movedNode.instance, movedNode.node);
				}
			}
			else {
				showAlert(lang.alert_copy);
			}
		}, 'json');

		$('#modalConfirmDnd').modal('hide');
	});

	// navrat spat
	$('#buttonDndCancel').on('click', function(e){
		// z ineho stromu
		if (oldMovedNode) {
			movedNode.new_instance.delete_node(movedNode.node);
		}
		// v tom istom strome
		else {
			movedNode.instance.move_node(movedNode.node, movedNode.old_parent, movedNode.old_position);
		}

		$('#modalConfirmDnd').modal('hide');
	});

	// vytvorenie regionu
	$('#createRegion').on('click', function(e){
		e.preventDefault();

		var ref = $('.tree:first').jstree(true);
		ref.create_node('#', { 'type': 'file' }, 'last');
	});

	// vytvorenie polozky
	$(document).on('click', 'a[href="#createItem"]', function(e){
		e.preventDefault();

		var inst = $.jstree.reference($(this).closest('div[data-jstree="true"]')),
			obj = inst.get_node( $(this).closest('li[role="treeitem"]').attr('id') );

		inst.create_node(obj, {}, 'last', function(new_node){
			setTimeout(function(){ inst.edit(new_node); }, 0);
		});
	});

	// vymazanie polozky
	$(document).on('click', 'a[href="#deleteItem"]', function(e){
		e.preventDefault();

		var inst = $.jstree.reference( $(this).closest('div[data-jstree="true"]') ),
			obj = inst.get_node( $(this).closest('li[role="treeitem"]').attr('id') );

		if(! confirm('Are you sure you want to delete item?')) {
			return;
		}

		$.post('/api/regions/' + obj.id + '/delete', {
			'item_id': obj.id
		}, function(d){
			if (d && d == '1') {
				if (inst.is_selected(obj)) {
					inst.delete_node(inst.get_selected());
				}
				else {
					inst.delete_node(obj);
				}

				syncTree('delete_node', inst, obj);
			}
			else {
				syncTree('delete_node', inst, obj);
				//showAlert(lang.alert_delete);
			}
		});
	});

	// editovanie polozky
	$(document).on('click', 'a[href="#editItem"]', function(e){
		e.preventDefault();

		var inst = $.jstree.reference( $(this).closest('div[data-jstree="true"]') ),
			obj = inst.get_node( $(this).closest('li[role="treeitem"]').attr('id') );

		if (obj.li_attr['data-category-type'].match(/(region|destination)/)) {
			$('#flightDurationBlock').show();
		} else {
			$('#flightDurationBlock').hide();
		}

		// nastavime titulok
		var title = '';
		var tmpObj;
		$.each(obj.parents, function(k, v){
			if (v == '#') return true;

			tmpObj = inst.get_node(v);

			title = tmpObj.text + ' &raquo; ' + title;
		});
		title = title + ' ' + obj.text;

		// GID
		if (obj.li_attr['data-category-type'] == 'hotel' && obj.li_attr['data-original-id'] != null) {
			title += ' (GID: ' + obj.li_attr['data-original-id'] + ')';
		}

		$('#modalEditItemTitle').html(title);

		// nacitanie dat v modalnom okne (ajax)
		loadItem(obj);
	});

	// zmena stavu (aktivny/neaktivny)
	$(document).on('click', 'a[href="#changeState"]', function(e){
		e.preventDefault();

		var link = $(this);
		var li = link.closest('li[role="treeitem"]');
		var inst = $.jstree.reference( link.closest('div[data-jstree="true"]') );
		var state = li.attr('data-active') == 1 ? 0 : 1;
		var itemId = li.attr('id');

		$.post('/api/regions/update/' + itemId + '/item-state', {
			'action' : 'setItemState',
			'state'  : state,
			'item_id': itemId
		}, function(d){
			if (d && d == '1') {
				if (state == 1) {
					link.addClass('active');
					li.attr('data-active', '1');

					syncTree('set_state', inst, { 'id': itemId }, 1);
				} else {
					link.removeClass('active');
					li.attr('data-active', '0');

					syncTree('set_state', inst, { 'id': itemId }, 0);
				}
			}
			else {
				showAlert(lang.alert_change_state);
			}
		});
	});

	// upload obrazka
	$('form.drop-zone').dropzone({
		url: './',
		acceptedFiles: '.jpg',
		//previewsContainer: 'div.previewItemImage',
		previewTemplate: '<div id="preview-template" style="display: none;"></div>',
		complete: function(file) {
			if (file && file.xhr) {
				var data = $.parseJSON(file.xhr.response);
				if (data.success) {
					appendImage(data);

					$('#previewItemImages').sortable();
				}
			}
			return false;
		}
	});

	// tinyMCE
	tinymce.init({
		selector: '#itemDescription',
		plugins : 'code',
		language: 'sk'
	});
	// prevent Bootstrap from hijacking TinyMCE modal focus
	$(document).on('focusin', function(e) {
		if ($(e.target).closest(".mce-window").length) {
			e.stopImmediatePropagation();
		}
	});

	// ulozenie polozky
	$('#saveItem').on('click', function(e){
		$.post('/api/regions/update/' + $('#saveItem').attr('data-id'), {
			'item_id'         : $('#saveItem').attr('data-id'),
			'description'     : tinyMCE.activeEditor.getContent(),
			'flight_duration' : $('#itemFlightDuration').val(),
			'meta_title'      : $('#metaTitle').val(),
			'meta_description': $('#metaDescription').val(),
			'meta_keywords'   : $('#metaKeywords').val(),
			'meta_url'        : $('#metaURL').val()
		}, function(d){
			if (d && d == '1') {
				$('#modalEditItem').modal('hide');
			}
			else {
				showAlert(lang.alert_save);
			}
		});
	});

	var imageActionIcons = {
		'delete': $('<a href="#deleteImage" class="imageActionIcon delete" title="' + lang.delete_image_title + '"><i class="fa fa-times-circle"></i></a>')
	};
	function appendImage(image) {
		var div = $('<div class="itemImage" data-image-id="' + image.id + '"><img src="' + image.url + '"></div>');
		div
			.hover(function(){
				$(this).append(imageActionIcons['delete']);
			}, function(){
				$('a.imageActionIcon').remove();
			})
			.appendTo('#previewItemImages');
	}

	// vymazanie obrazka
	$(document).on('click', 'a[href="#deleteImage"]', function(e){
		e.preventDefault();

		var imgEl = $(this).closest('div.itemImage');

		$('#modalConfirmQuestion').html(lang.confirm_delete_image);
		$('#modalConfirmSuccess').unbind('click').on('click', function(){

			$.post('', {
				'action'         : 'deleteItemImage',
				'item_id'        : $('#saveItem').attr('data-id'),
				'image_id'       : imgEl.attr('data-image-id')
			}, function(d){
				if (d && d == '1') {
					imgEl.fadeOut('fast');

					$('#modalConfirm').modal('hide');
				}
				else {
					showAlert(lang.alert_delete_image);
				}
			});
		});

		$('#modalConfirm').modal({
			'keyboard': false,
			'backdrop': 'static'
		});
	});

	function loadItem(obj) {

		// default hodnoty
		tinyMCE.activeEditor.setContent('');
		$('#itemFlightDuration').val('');

		// buttonu na ulozenie pridame ID
		$('#saveItem').attr('data-id', obj.id);
		$('#previewItemIdImage').val(obj.id);

		$.get('/api/regions/' + obj.id, function(d){
			if (d && d.item) {

				// obrazky
				$.each(d.item.images, function(k, image){
					appendImage(image);
				});

				// zoradovanie obrazkov
				$('#previewItemImages').sortable({
					placeholderClass: 'itemImagePlaceholder',
				})
				.on('sortupdate', function(e, ui){
					$.post('', {
						'action'      : 'reorderItemImage',
						'item_id'     : obj.id,
						'image_id'    : $(ui.item).attr('data-image-id'),
						'new_position': ui.index
					}, function(d){
						if (!d || d != '1') {
							showAlert(lang.alert_reorder_image);
						}
					});
				});

				// popis
				if (d.item.description) {
					tinyMCE.activeEditor.setContent(d.item.description);
				}
				// trvanie letu
				if (d.item.flight_duration) {
					$('#itemFlightDuration').val(d.item.flight_duration);
				}
				// seo
				$('#metaTitle').val(d.item.seo_title);
				$('#metaDescription').val(d.item.seo_description);
				$('#metaKeywords').val(d.item.seo_keywords);
				$('#metaURL').val(d.item.seo_url);

				if (d.item.seo_title || d.item.seo_description || d.item.seo_keywords || d.item.seo_url) {
					$('#editItemTablist a[href="#editItemTabSeo"]').prepend('<i class="fa fa-plus pr5"></i>');
				} else {
					$('#editItemTablist a[href="#editItemTabSeo"] i').remove();
				}

			}
			else {
				showAlert(lang.alert_load_item);
			}
		}, 'json');

		$('#modalEditItem').modal({
			'keyboard': false,
			'backdrop': 'static'
		});
	}

	$('#modalEditItem').on('show.bs.modal', function(){
		$('#editItemTablist > li:first > a').tab('show');
	});
	$('#modalEditItem').on('hidden.bs.modal', function(){
		$('#previewItemImages').sortable('destroy').unbind('sortupdate');
		$('#previewItemImages').html('');
	});

	// zobrazenie warningu
	function showAlert(message) {
		$('#modalWarningMessage').html(message);

		$('#modalAlert').modal({
			'keyboard': false,
			'backdrop': 'static'
		});
	}

	function syncTree(action, inst, obj, data) {
		var anotherInst = $.jstree.reference('#tree_left');
		if (anotherInst === inst) {
			anotherInst = $.jstree.reference('#tree_right');
		}
		var anotherObj = anotherInst.get_node(obj.id);

		syncAction = action;

		// premenovanie
		if (action == 'rename_node') {
			if (!anotherObj)
					return syncAction = false;

			anotherInst.rename_node(anotherObj, obj.text);
		}
		// kopirovanie
		else if (action == 'copy_node') {

		}
		// presun
		else if (action == 'move_node') {

		}
		// zmena poradia
		else if (action == 'reorder_node') {
			if (data.position > data.old_position)
				data.position++;

			// presun medzi stromami
			if (oldMovedNode) {
				inst.delete_node(obj);

				inst.move_node(oldMovedNode, data.parent, data.position, function(){
					setTimeout(function(){
						anotherObj = anotherInst.get_node(oldMovedNode.id);

						syncAction = action;
						anotherInst.move_node(anotherObj, data.parent, data.position);
					}, 0);
				});

			}
			// presun v tom istom strome
			else {
				if (!anotherObj)
					return syncAction = false;

				anotherInst.move_node(anotherObj, data.parent, data.position);
			}
		}
		// vymazanie
		else if (action == 'delete_node') {
			anotherInst.delete_node(anotherObj);
		}
		// nastavenie aktivny/neaktivny
		else if (action == 'set_state') {
			var li = $(anotherInst.element).find('#' + obj.id);
			if (li) {
				if (data) {
					li.attr('data-active', '1');
					li.children('a[href="#changeState"]').addClass('active');
				} else {
					li.attr('data-active', '0');
					li.children('a[href="#changeState"]').removeClass('active');
				}
			}
		}
		// vytvorenie
		else if (action == 'create_node') {
			var anotherParent = anotherInst.get_node(data.parent);

			var newId = anotherInst.create_node(data.parent, {}, data.position, null, true);
			var newObj = anotherInst.get_node(newId);

			anotherInst.set_id(newObj, data.node.id);
			newObj.li_attr['data-custom'] = data.node.li_attr['data-custom'];
			newObj.li_attr['data-active'] = data.node.li_attr['data-active'];
			newObj.li_attr['data-category-id'] = data.node.li_attr['data-category-id'];
			newObj.li_attr['data-category-type'] = data.node.li_attr['data-category-type'];
		}
	}

	$(document).on('dnd_start.vakata', function(){
		oldMovedNode = false;
	});

	// po zmene velkosti browsera nastavime vysku stromov
	$(window).resize(function() {
		var screenHeight = parseInt( $(window).height() );
		var topbarHeight = parseInt( $('div.top-box').outerHeight() );

		$('div.tree').css('height', screenHeight - topbarHeight - 65);
	}).resize();


	// vyhladavanie autocomplete
	var autocomplete = {
		'ulSearchResults': $('#searchResults'),
		'searchTextInput': $('#searchText'),
		'init': function(){
			var autocomplete = this;
			var ulSearchResults = autocomplete.ulSearchResults;
			var searchTextInput = autocomplete.searchTextInput;

			// focusom zobrazime posledne vysledky
			this.searchTextInput.on('focus', function(e){
				if (ulSearchResults.children('li').length > 0) {
					ulSearchResults.show();
				}
			});

			// stlacenie klavesu alebo focus
			this.searchTextInput.on('keyup', function(e){
				if (e.which == 38 || e.which == 40) return;

				var val = $(this).val().trim();
				if (val.length < 3) {
					ulSearchResults.hide();
					return;
				}

				$.get('/api/regions/search?query=' + val, function(d){
					ulSearchResults.html('');

					var searchRe = new RegExp(val, 'i');

					var results = [];

					// predpriprava vysledkov
					if (d.results.length > 0) {
						results = [];
						$.each(d.results, function(k, v){
							if (!results[v.category_type])
								results[v.category_type] = [];

							results[v.category_type].push(v);
						});
					}

					if (Object.keys(results).length > 0) {
						$.each(Object.keys(results), function(k, categoryType){

							ulSearchResults.append('<li class="dropdown-header">' + categoryType + '</li>');

							$.each(results[categoryType], function(k, item){
								item.name = item.name.replace(searchRe, '<strong>' + val + '</strong>');
								ulSearchResults.append('<li class="resultItem"><a href="#goSearch" data-id="' + item.id + '">' + item.name + '</a></li>');
							});
						});
					} else {
						ulSearchResults.append('<li class="text-center">' + lang.no_results + '</li>');
					}

					ulSearchResults.show();
				}, 'json');
			});

			// vyhladavanie vysledkov
			$('#doSearchButton').on('click', function(e){
				e.preventDefault();

				showAlert('Vyhľadávanie je momentálne v BETA verzii, pre zobrazenie výsledku kliknite na výsledok v našepkávači.<br><small>Na zdokonalení tohto produktu neustále pracujeme, čoskoro pribudne rozšírené vyhľadávanie.</small>');
			});

			// stlacenie klavesu
			this.searchTextInput.on('keydown', function(e){
				// enter
				if (e.which == 13) {
					e.preventDefault();
					$('#searchResults li.active').children('a').trigger('click');
					$(this).blur();
					return false;
				}

				// sipky hore a dole
				if (e.which == 38 || e.which == 40) {
					e.preventDefault();

					var activeRow = $('#searchResults li.active');
					if (activeRow.length > 0)
						activeRow.removeClass('active');
					else
						return $('#searchResults li.resultItem:first').addClass('active');

					// hore
					if (e.which == 38) {
						activeRow = activeRow.prev('li.resultItem');

						if (activeRow.length == 0)
							return $('#searchResults li.resultItem:first').addClass('active');
					}
					// dole
					if (e.which == 40) {
						activeRow = activeRow.next('li.resultItem');

						if (activeRow.length == 0)
							return $('#searchResults li.resultItem:last').addClass('active');
					}

					activeRow.addClass('active');
				}

			});
			// po odideni z policka skryjeme nasepkavac
			this.searchTextInput.on('blur', function(){
				setTimeout(function(){ ulSearchResults.hide(); }, 200);
			});

			$(document).on('click', 'a[href="#goSearch"]', function(e){
				e.preventDefault();

				// vybranu polozku dame do policka
				searchTextInput.val( $(this).text() );
				// skryjeme dropdown
				ulSearchResults.hide().html('');

				var itemId = $(this).attr('data-id');

				$.get('/api/regions/' + itemId + '?get_parents=1', function(d){
					if (d && d.item) {
						autocomplete.open_node(d.item.parents, itemId);
					}
				}, 'json');
			});
		},
		'open_node': function(parents, lastId){
			var autocomplete = this;

			var itemId = parents.shift();
			if (!itemId) {
				$.jstree.reference('#tree_left').select_node(lastId);
				$('#tree_left').scrollTop(parseInt($('#tree_left #' + lastId).offset().top) - parseInt($('#tree_left').height()) / 2);
				return;
			}

			var inst = $.jstree.reference('#tree_left'),
				obj = inst.get_node(itemId);

			inst.open_node(obj, function(d){
				// nacitanie dalsej polozky
				if (parents.length > 0) {
					autocomplete.open_node(parents, lastId);
				}
				// zobrazenie vysledku
				else {
					if (selectedResultNode) {
						inst.deselect_node(selectedResultNode);
					}

					// vycentrujeme na vysledok
					$('#tree_left').scrollTop(parseInt($('#tree_left #' + lastId).offset().top) - parseInt($('#tree_left').height()) / 2);
					//console.log(parseInt($('#tree_left #' + lastId).offset().top));
					//console.log(parseInt($('#tree_left').height()) / 2);

					selectedResultNode = lastId;
					inst.select_node(lastId);
				}
			});
		}
	};
	autocomplete.init();

});