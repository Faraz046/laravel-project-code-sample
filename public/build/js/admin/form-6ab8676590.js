!function( $ ){

    "use strict";

    $(function () {

        /**
         * Delete attachment
         */
        $('.delete-attachment').click(function(){

            var field  = $(this).data('field'),
                id     = $(this).data('id'),
                table  = $(this).data('table'),
                data   = {},
                $this  = $(this),
                url    = '/api/' + table + '/' + id;

            if (! confirm('Delete ' + field + '?')) {
                return false;
            }

            data['id'] = id;
            data[field] = 'delete';

            $.ajax({
                type: 'PUT',
                url: url,
                data: data
            }).done(function() {
                $this.parent().remove();
            }).fail(function () {
                alertify.error('An error occurred while deleting attachment.');
            });

            return false;
        });

        /**
         * Crop attachment
         */
         $('.crop-attachment').on('click', function() {
            var that = this;
            var id = $(this).data('id');
            var url = '/api/' + $(this).data('table') + '/' + id + '/crop';

            var originalImage = document.createElement('img');
            originalImage.src = $(this).data('src');
            originalImage.style.maxWidth = '100%';

            var cropInfo = undefined;
            var dialog = vex.dialog.open({
                overlayClosesOnClick: false,
                message: '',
                input: originalImage,
                buttons: [
                    $.extend({}, vex.dialog.buttons.YES, { text: 'Save' }),
                    vex.dialog.buttons.NO,
                ],
                afterOpen: function(context) {
                    $(originalImage).cropper({
                        zoomable: false,
                        crop: function(e) { cropInfo = e; }
                    });
                },
                afterClose: function() {
                    $(originalImage).cropper('destroy');
                },
                callback: function(data) {
                    if (!data) return;
                    alertify.log("Saving image...");

                    var file = dataUriToBlob($(originalImage).cropper('getCroppedCanvas').toDataURL("image/jpeg", 0.90));

                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: (function(fd) {
                            var filename = strRand(10) + file.extension;
                            fd.append('id', id);
                            fd.append('width', cropInfo.width);
                            fd.append('height', cropInfo.height);
                            fd.append('filename', filename);
                            fd.append('file', file.blob, filename);
                            return fd;
                        })(new FormData),
                        processData: false,
                        contentType: false
                    }).done(function(response) {
                        alertify.success('Image was saved successfully.');
                        $(that).data('src', response.src);
                        $(that).find('img').attr('src', response.src);
                    }).fail(function () {
                        alertify.error('An error occurred while saving image.');
                    });
                }
            });
        });

        /**
         * Selectize for select input
         */
        $('select#galleries').selectize();
        $('select#category_id').selectize();
        $('select#page_id').selectize();
        $('select#target').selectize();

        /**
         * Selectize for tags
         */
        if ($('#tags').length) {
            $.ajax({
                type: 'GET',
                url: '/api/tags'
            }).done(function(data) {
                var tags = data.map(function(x) { return { item: x.tag }; });
                $('#tags').selectize({
                    persist: false,
                    create: true,
                    delimiter: ', ',
                    options: tags,
                    searchField: ['item'],
                    labelField: 'item',
                    valueField: 'item',
                    createOnBlur: true
                });
            }).fail(function () {
                alertify.error('An error occurred while getting tags.');
            });
        }

        /**
         * Set button in red on validation errors
         */
        var firstErrorTabActive = false;
        $('.tab-pane').each(function(index, el) {
            if ($(this).find('.has-error').length) {
                var tabButton = $('a[data-target="#' + $(this).attr('id') + '"]');
                if ( ! firstErrorTabActive) {
                    tabButton.tab('show');
                    firstErrorTabActive = true;
                }
                var dangerClass = 'text-danger';
                if (tabButton.hasClass('btn')) {
                    dangerClass = 'btn-danger';
                }
                tabButton.addClass(dangerClass);
            };
        });

        /**
         * Locale switcher : set active button
         */
        $('#btn-group-form-locales .btn').click(function(){
            $(this).parent().children('.active').removeClass('active');
            $(this).addClass('active');
        });

    });

}( window.jQuery || window.ender );
