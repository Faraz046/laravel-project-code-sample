<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use TypiCMS\Modules\Companies\Models\Company;
use TypiCMS\Modules\Touroperators\Models\Touroperator;

class GenerateTouroperators extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var \TypiCMS\Modules\Companies\Models\Company
     */
    private $company;

    /**
     * Create a new job instance.
     * @param \TypiCMS\Modules\Companies\Models\Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $touroperators = [
            ['company_id' => $this->company->id, 'code' => 'ALL-SK', 'name' => 'SLOVENSKÉ CK', 'country' => null, 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ALL-AT', 'name' => 'RAKÚSKE CK', 'country' => null, 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FLY', 'name' => '1,2 Fly', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'FLYD', 'name' => '1,2 Fly', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'FLY,FLYD', 'name' => '1,2 Fly', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => '5VF', 'name' => '5 vor Flug', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ACT', 'name' => 'Action Sport', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ADAC', 'name' => 'ADAC Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ADDB', 'name' => 'Adriadatabanka', 'country' => 'CZ', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ADRB', 'name' => 'Adrialand', 'country' => 'CZ', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'AEOL', 'name' => 'Aeolus', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'AB', 'name' => 'Air Berlin', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'AIR', 'name' => 'Air Marin', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ALD', 'name' => 'Aldiana', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ALL', 'name' => 'Alltours', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'XALL', 'name' => 'Alltours', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'ALL,XALL', 'name' => 'Alltours', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'AME', 'name' => 'Ameropa', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ATT', 'name' => 'ATT Touristik', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ATK', 'name' => 'Attika Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'AZUR', 'name' => 'Azur Reizen', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'BAVA', 'name' => 'Bavaria Fernreisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'BCH', 'name' => 'Bentour Swiss', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'BGT', 'name' => 'BG Tours', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'BIG,XBIG', 'name' => 'BigXtra', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'BIG', 'name' => 'BigXtra', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'XBIG', 'name' => 'BigXtra', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'BTM', 'name' => 'Bodenmais Tourismus und Marketing GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'BU', 'name' => 'Bucher Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'BUC', 'name' => 'Buchmal Reisen GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'BYE', 'name' => 'byebye GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'CLT', 'name' => 'Classic Tours GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'CBM', 'name' => 'Club Blaues Meer', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'CFI', 'name' => 'Condor', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'DEM', 'name' => 'Demed', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'DER', 'name' => 'DER Tour', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'DIS', 'name' => 'Discount Travel', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FEDE', 'name' => 'DtF Travel GmbH (FeriDe)', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'DTI', 'name' => 'Dutch Travel International', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ECC', 'name' => 'Ecco Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ETI', 'name' => 'ETI', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ERBE', 'name' => 'Eurorelais', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'EXPT', 'name' => 'Experitour', 'country' => 'CZ', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FEFA', 'name' => 'Fefa.Reisen GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FER', 'name' => 'Ferien Touristik GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FIRO', 'name' => 'FIRO-tour (SK)', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FRCZ', 'name' => 'FIRO-tour (CZ)', 'country' => 'CZ', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FIT', 'name' => 'FIT Gesellschaft für gesundes Reisen mbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FTG', 'name' => 'FTG GmbH Fox Travel Germany', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FTI', 'name' => 'FTI', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'XFTI', 'name' => 'XFTI', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'FTI,XFTI', 'name' => 'FTI', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SKY', 'name' => 'German Sky', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => '4U', 'name' => 'Germanwings', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'GLA', 'name' => 'Glauch Reisen GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'GLOB', 'name' => 'Globtour', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'GTI', 'name' => 'GTI Travel', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'GTTS', 'name' => 'Gulet Club Magic Life', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'GULA', 'name' => 'Gulet Touristik', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'GULE', 'name' => 'Gulet Touristik', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'GULA,GTTS', 'name' => 'Gulet Touristik', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'HHT', 'name' => 'H&amp;H Tur Touristik GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'HAPP', 'name' => 'Happy Travel', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'HECH', 'name' => 'Hechter', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'HERM', 'name' => 'Hermes Touristik', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'HUC', 'name' => 'Holiday &amp; Co', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'HYDR', 'name' => 'Hydrotour', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'IBO', 'name' => 'Ibero Tours GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'IHOM', 'name' => 'Interhome', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ICC', 'name' => 'Involatus Carrier Consulting GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ITS', 'name' => 'ITS', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ITSX', 'name' => 'ITS Indi / ITSX', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ITSB', 'name' => 'ITS Österreich', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ITT', 'name' => 'ITT Touristik', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'JAHN,XJAH', 'name' => 'JAHN Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'JAHN', 'name' => 'JAHN Reisen', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'XJAH', 'name' => 'JAHN Reisen', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'JANA,XJAH', 'name' => 'JAHN Reisen Österreich', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'JANA', 'name' => 'JAHN Reisen Österreich', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'JT', 'name' => 'JT Touristik GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'KART', 'name' => 'Kartago Tours', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'KOAL', 'name' => 'Koala Tours', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'KONT', 'name' => 'KM Travel', 'country' => 'CZ', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'LMX', 'name' => 'LMX Touristik', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'LOW', 'name' => 'lowcostbeds.de', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'MARO', 'name' => 'Maro Touristik', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'MED', 'name' => 'Medina (D)', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'MPR', 'name' => 'Mediplus Reisen GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'MWR', 'name' => 'Meiers Weltreisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'MERS', 'name' => 'Merson/RTK', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'MISR', 'name' => 'MISR Travel ägyptische AG', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'MON', 'name' => 'Mondial GmbH &amp; Co.KG', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'NEC', 'name' => 'Neckermann', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'NER', 'name' => 'Neckermann', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'NEC,NER', 'name' => 'Neckermann', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'CEN', 'name' => 'Neckermann Center Parcs', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'OES', 'name' => 'Neckermann Österreich', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'OFT', 'name' => 'OFT Reisen GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'OGE', 'name' => 'Öger Tours', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'OGO', 'name' => 'Öger Tours Österreich', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'OETT', 'name' => 'Öger Türktur', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'OLI', 'name' => 'Olimar Flugreisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'OHD', 'name' => 'On Holiday Group Ltd.', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'XPOD', 'name' => 'Opodo Tours GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'OREX', 'name' => 'Orex', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'PGS', 'name' => 'Pegasus Airlines', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'PHX', 'name' => 'Phoenix Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'RAIN', 'name' => 'RAINBOW Tours', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'FALK', 'name' => 'Reisefalke', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'RENY', 'name' => 'Reny Travel', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'RIVA', 'name' => 'Riva Tours', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'RUE1', 'name' => 'Ruefa Reisen', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SATU', 'name' => 'SATUR', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SNCA', 'name' => 'Seneca Tours', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SLR', 'name' => 'Schauinsland Reisen', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'SLRD', 'name' => 'Schauinsland Reisen', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'SLR,SLRD', 'name' => 'Schauinsland Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SR', 'name' => 'Schmetterling Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SRF', 'name' => 'Schmetterling Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SIT', 'name' => 'SITALIA Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SMAP', 'name' => 'Smartapart GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SNOW', 'name' => 'SnowTrex (TravelTrex GmbH)', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SOL', 'name' => 'Sollymar GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SLVX', 'name' => 'Solvex', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'SPTS', 'name' => 'Sport-S', 'country' => 'CZ', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TREX', 'name' => 'SummerTrex (TravelTrex GmbH)', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ST', 'name' => 'SunTrips', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TAM', 'name' => 'Tamira Travel', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TERR', 'name' => 'Terra Reisen', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TOC', 'name' => 'Thomas Cook', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TIP', 'name' => 'Thomas Cook Flug', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TIME', 'name' => 'Time Travel', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TIP2', 'name' => 'TIP Travel', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TJAE', 'name' => 'Tjaereborg', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TSS', 'name' => 'Touristik Service Schürmann', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TOUR', 'name' => 'Touropa', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TSYS', 'name' => 'Toursys', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TUI', 'name' => 'TUI Deutschland', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'TUID', 'name' => 'TUI Deutschland', 'country' => 'DE', 'active' => 0],
            ['company_id' => $this->company->id, 'code' => 'TUI,TUID', 'name' => 'TUI Deutschland', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'HLF', 'name' => 'TUI Fly&amp;More', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TUIA', 'name' => 'TUI Österreich', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'TURA', 'name' => 'Turancar', 'country' => 'SK', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'ULT', 'name' => 'Urlaubstours GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'VTO', 'name' => 'Vtours GmbH', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'WTA', 'name' => 'Worldwide Events', 'country' => 'DE', 'active' => 1],
            ['company_id' => $this->company->id, 'code' => 'XDT', 'name' => 'X Dreams Travel', 'country' => 'DE', 'active' => 1],
        ];

        foreach($touroperators as $data) {
            $model = new Touroperator();
            $model->fill($data);
            $model->save();
        }
    }
}
