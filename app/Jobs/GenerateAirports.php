<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use TypiCMS\Modules\Airports\Models\Airport;
use TypiCMS\Modules\Companies\Models\Company;

class GenerateAirports extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var \TypiCMS\Modules\Companies\Models\Company
     */
    private $company;

    /**
     * Create a new job instance.
     * @param \TypiCMS\Modules\Companies\Models\Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $airports = [
            ['company_id' => $this->company->id, 'code' => 'ALL-EU', 'title' => '--nerozhoduje--', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'ALL-DEF', 'title' => 'Bratislava, Viedeň, Praha, Budapešť', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'ALL-SK', 'title' => 'Všetky slovenské mestá', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'ALL-CZ', 'title' => 'Všetky české mestá', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'ALL-AT', 'title' => 'Všetky rakúske mestá', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'VIE', 'title' => 'Viedeň', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'BTS', 'title' => 'Bratislava', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'KSC', 'title' => 'Košice', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'SLD', 'title' => 'Sliač', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'TAT', 'title' => 'Poprad-Tatry', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'PRG', 'title' => 'Praha', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'BRQ', 'title' => 'Brno', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'OSR', 'title' => 'Ostrava', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'BUD', 'title' => 'Budapest', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'KTW', 'title' => 'Katowice', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'KRK', 'title' => 'Krakow', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'GRZ', 'title' => 'Graz', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'LNZ', 'title' => 'Linz', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'SZG', 'title' => 'Salzburg', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'INN', 'title' => 'Innsbruck', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'MUC', 'title' => 'Mníchov', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'FRA', 'title' => 'Frankfurt', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'NUE', 'title' => 'Nürnberg', 'active' => true],
            ['company_id' => $this->company->id, 'code' => 'DRS', 'title' => 'Dresden', 'active' => true],
        ];

        foreach($airports as $data) {
            $airport = new Airport();
            $airport->fill($data);
            $airport->save();
        }
    }
}
