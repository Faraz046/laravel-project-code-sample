<?php

namespace App\Policies;

use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Grant all privileges to owner user
     * @param $ability
     * @return bool
     */
    public function before($ability)
    {
        return (bool)object_get(Auth::user(), 'owner') === true;
    }

    /**
     * Check if user can create companies
     * @return bool
     */
    public function create()
    {
        return (bool)object_get(Auth::user(), 'owner') === true;
    }
}
