<?php

namespace App\Providers;

use Auth;
use TypiCMS\Modules\News\Models\News;
use TypiCMS\Modules\Pages\Models\Page;
use TypiCMS\Modules\Menus\Models\Menu;
use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Slides\Models\Slide;
use TypiCMS\Modules\Blocks\Models\Block;
use TypiCMS\Modules\Files\Models\File;
use TypiCMS\Modules\Galleries\Models\Gallery;

class CompanyProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Page::creating(function($page) {
            if(! Auth::check()) {
                return false;
            }

            if(Auth::user()->owner === false && ! is_null(Auth::user()->company)) {
                $page->company_id = Auth::user()->company_id;
            }
        });

        News::creating(function($news) {
            if(! Auth::check()) {
                return false;
            }

            if(Auth::user()->owner === false && ! is_null(Auth::user()->company)) {
                $news->company_id = Auth::user()->company_id;
            }
        });

        Menu::creating(function($menu) {
            if(! Auth::check()) {
                return false;
            }

            if(Auth::user()->owner === false && ! is_null(Auth::user()->company)) {
                $menu->company_id = Auth::user()->company_id;
            }
        });

        File::creating(function($file) {
            if(! Auth::check()) {
                return false;
            }

            if(Auth::user()->owner === false && ! is_null(Auth::user()->company)) {
                $file->company_id = Auth::user()->company_id;
            }
        });

        Gallery::creating(function($gallery) {
            if(! Auth::check()) {
                return false;
            }

            if(Auth::user()->owner === false && ! is_null(Auth::user()->company)) {
                $gallery->company_id = Auth::user()->company_id;
            }
        });

        Block::creating(function($block) {
            if(! Auth::check()) {
                return false;
            }

            if(Auth::user()->owner === false && ! is_null(Auth::user()->company)) {
                $block->company_id = Auth::user()->company_id;
            }
        });

        Slide::creating(function($slide) {
            if(! Auth::check()) {
                return false;
            }

            if(Auth::user()->owner === false && ! is_null(Auth::user()->company)) {
                $slide->company_id = Auth::user()->company_id;
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
