<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use TypiCMS\Modules\Menus\Models\Menulink;
use TypiCMS\Modules\Slides\Models\Slide;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Slide::saving(function($slide) {
            if(! starts_with($slide->url, ['http://', 'https://'])) {
                $slide->url = 'http://' . $slide->url;
            }
        });

        Menulink::saving(function($menulink) {
            if(! starts_with($menulink->url, ['http://', 'https://'])) {
                $menulink->url = 'http://' . $menulink->url;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }
    }
}
