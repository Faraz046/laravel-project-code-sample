/**
 * Convert Data URI into Blob.
 * Provide MIME type and extenstion for more info.
 */
function dataUriToBlob (dataUri) {
    function parseMimeType(uri) {
        return uri.substring(5, uri.indexOf(';'))
    }

    function normalizeMimeType(mime) {
        var prefix = /^(\w+\/)+/
        mime = mime.toLowerCase()
        var once = mime.match(prefix)
        if (!once || !(once = once[1])) {
            return mime;
        }

        return mime.replace(prefix, once)
    }

    function normalizeExtension(mimeType) {
        var ext = mimeType.split('/')[1];

        if (ext === 'jpeg') {
            return '.jpg';
        }

        return '.' + ext;
    }

    function toBlob(dataUri, mimeType)
    {
        var blobBin = atob(dataUri.split(',').pop());
        var array = [];

        for (var i = 0; i < blobBin.length; i++) {
            array.push(blobBin.charCodeAt(i));
        }

        return new Blob([new Uint8Array(array)], {type: mimeType});
    }

    var mimeType = normalizeMimeType(parseMimeType(dataUri));
    return {
        mimeType: mimeType,
        extension: normalizeExtension(mimeType),
        blob: toBlob(dataUri, mimeType)
    };
}

/**
 * Generate random string with given length (5 chars as default).
 */
function strRand(l)
{
    var length = l || 5;
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    var str = "";
    for( var i=0; i < length; i++ ) {
        str += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return str;
}
