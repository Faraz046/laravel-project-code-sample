<?php

return [

    /*
    |--------------------------------------------------------------------------
    | EN Global TypiCMS Language Lines
    |--------------------------------------------------------------------------
    */
    'languages' => [
        'fr'  => 'French',
        'es'  => 'Spanish',
        'nl'  => 'Dutch',
        'en'  => 'Angličtina',
        'all' => 'All languages',
        'sk' => 'Slovenčina'
    ],
    'form' => [
        'page content' => 'page content',
        'meta data'    => 'meta data',
        'options'      => 'options',
    ],
    'menus' => [
        'content'  => 'Obsah',
        'users'    => 'Používatelia a role',
        'media'    => 'Media',
        'contacts' => 'Contacts',
    ],
    'Index'        => 'List',
    'Create'       => 'Create',
    'Store'        => 'Store',
    'Update'       => 'Update',
    'Delete'       => 'Vymazať',
    'Select all'   => 'Select all',
    'Deselect all' => 'Deselect all',
    'Online'       => 'Online',
    'Offline'      => 'Offline',
    'View'         => 'View',
    'Sort'         => 'Sort',
    'Edit'         => 'Edit',
    'Search'       => 'Search',
    'Not found'    => 'Not found',

    'Yes' => 'Yes',
    'No'  => 'No',

    'Home'                  => 'Home',
    'En ligne/Hors ligne'   => 'Online/Offline',
    'No default page found' => 'No default page found',
    'No file'               => 'No file',
    'Settings'              => 'Nastavenia',
    'Admin side'            => 'Admin side',
    'View website'          => 'Zobraziť stránku',

    'Mandatory fields' => 'Mandatory fields',

    // Tabs
    'Content'   => 'Obsah',
    'Meta'      => 'Meta',
    'Options'   => 'Nastavenia',
    'Galleries' => 'Galleries',
    'Files'     => 'Files',
    'Images'    => 'Images',
    'Info'      => 'Info',

    'Toggle navigation' => 'Toggle navigation',

    'Items sorted' => 'Items sorted.',

    'Security token expired. Please, repeat your request.' => 'Security token expired. Please, repeat your request.',

    //pages
    'Pages' => 'Stránky',
];
