@extends('core::admin.master')

@section('title', trans('global.Settings'))

@section('main')

<h1>@lang('global.Settings')</h1>

<div class="row">

    <div class="col-sm-6">

    {!! BootForm::open()->multipart() !!}
    {!! BootForm::bind($data) !!}

        @include('settings::admin._form')

    {!! BootForm::close() !!}

    </div>



</div>

@endsection
