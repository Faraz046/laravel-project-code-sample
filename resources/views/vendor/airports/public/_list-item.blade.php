<li>
    <a href="{{ route($lang.'.airports.slug', $airport->slug) }}" title="{{ $airport->title }}">
        {!! $airport->title !!}
        {!! $airport->present()->thumb(null, 200) !!}
    </a>
</li>
