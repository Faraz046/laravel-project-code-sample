@section('js')
    <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/admin/form.js') }}"></script>
@endsection

@include('core::admin._buttons-form')

{!! BootForm::hidden('id') !!}

@include('core::admin._image-fieldset', ['field' => 'image'])

@include('core::form._title-and-slug')
{!! TranslatableBootForm::hidden('status')->value(0) !!}
{!! BootForm::text('Code', 'code') !!}
{!! BootForm::select('Position', 'position', $positions)->value($model ? $model->position : '') !!}
{!! BootForm::hidden('active')->value(0) !!}
{!! BootForm::checkbox('Active', 'active') !!}
{!! TranslatableBootForm::checkbox(trans('validation.attributes.online'), 'status') !!}