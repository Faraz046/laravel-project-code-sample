@section('js')
    <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/admin/form.js') }}"></script>
@endsection

@include('core::admin._buttons-form')

{!! BootForm::hidden('id') !!}

@include('core::admin._image-fieldset', ['field' => 'image'])

{!! TranslatableBootForm::text(trans('validation.attributes.name'), 'name') !!}
{!! BootForm::hidden('status')->value(0) !!}
{!! BootForm::checkbox(trans('validation.attributes.online'), 'status') !!}
{!! BootForm::select('Position', 'position', $positions)->value($model ? $model->position : '') !!}