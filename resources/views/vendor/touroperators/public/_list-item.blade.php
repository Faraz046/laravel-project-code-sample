<li>
    <a href="{{ route($lang.'.touroperators.slug', $touroperator->slug) }}" title="{{ $touroperator->title }}">
        {!! $touroperator->title !!}
        {!! $touroperator->present()->thumb(null, 200) !!}
    </a>
</li>
