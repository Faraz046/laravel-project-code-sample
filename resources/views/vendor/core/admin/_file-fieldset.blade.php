        <div class="fieldset-media fieldset-file">
            @if($model->$field)
            <div class="fieldset-preview">
                @if ($model->type == 'i')
                    <div class="crop-attachment" data-src="{{ url($model->src) }}" data-table="{{ $model->getTable() }}" data-id="{{ $model->id }}">
                        <img src="{{ url($model->src) }}" height="150" />
                        <div class="fa-overlay">
                            <i class="fa fa-crop"></i>
                        </div>
                    </div>
                @else
                    {!! $model->present()->icon(2, $field) !!}
                @endif
                <small class="text-danger delete-attachment" data-table="{{ $model->getTable() }}" data-id="{{ $model->id }}" data-field="{{ $field }}">@lang('global.Delete')</small>
            </div>
            @endif
            <div class="fieldset-field">
                {!! BootForm::file(trans('validation.attributes.' . $field), $field) !!}
            </div>
        </div>
