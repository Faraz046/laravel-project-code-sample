<li>
    <a href="{{ route($lang.'.regions.slug', $region->slug) }}" title="{{ $region->title }}">
        {!! $region->title !!}
        {!! $region->present()->thumb(null, 200) !!}
    </a>
</li>
