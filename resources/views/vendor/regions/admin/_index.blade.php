@section('js')
    <!-- JS tree -->
    <script src="/js/public/tree/jstree.min.js"></script>
    <link rel="stylesheet" href="/js/public/tree/themes/default/style.min.css" />
    <link rel="stylesheet" href="/js/public/tree/style.css" />

    <!-- TinyMCE -->
    <script src="/js/public/tree/tinymce.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="/js/public/tree/bootstrap.min.js"></script>
    <script src="/js/public/tree/sortable.js"></script>

    <!-- Custom -->
    <script src="/js/public/tree/custom.js"></script>
@endsection

<div class="container">
    <div class="row top-box">
        <div class="col-md-12">
            <div class="form-group mb0 pull-left">
                <input type="text" class="form-control" id="searchText" placeholder="Search" data-provide="typeahead" autocomplete="off">
                <ul class="dropdown-menu" id="searchResults"></ul>
            </div>
            <button type="button" class="btn btn-info pull-left ml10" id="doSearchButton">
                <i class="fa fa-search"></i> Search
            </button>
            <span class="btn-separator"></span>
            <button type="button" class="btn btn-success pull-left" id="createRegion">
                <i class="fa fa-plus-square"></i>
                Create Region
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 tree" data-jstree="true" id="tree_left"></div>
        <div class="col-md-6 tree" data-jstree="true" id="tree_right"></div>
    </div>
</div>

<!-- modalne okno pre editaciu regionu/destinacie/mesta/hotela -->
<div class="modal full-width" tabindex="-1" role="dialog" id="modalEditItem">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalEditItemTitle"></h4>
            </div>
            <div class="modal-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="editItemTablist">
                    <li role="presentation" class="active"><a href="#editItemTabBasic" aria-controls="basic" role="tab" data-toggle="tab">Basic</a></li>
                    <li role="presentation"><a href="#editItemTabSeo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="editItemTabBasic">

                        <div class="row mb20">
                            <div class="col-md-12">
                                <div class="dropZoneWrapper">
                                    <form action="./" class="drop-zone">
                                        <input type="hidden" name="action" value="uploadImage">
                                        <input type="hidden" name="item_id" id="previewItemIdImage" value="">

                                        Upload image

                                        <div class="fallback">
                                            <input name="itemImage" type="file" multiple>
                                        </div>
                                    </form>
                                </div>

                                <div id="previewItemImages"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="itemDescription">Description:</label>
                                    <textarea id="itemDescription"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="flightDurationBlock">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="itemFlightDuration">Flight duration:</label>
                                    <input type="text" class="form-control" id="itemFlightDuration" placeholder="Flight duration...">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="editItemTabSeo">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="metaTitle">Meta title:</label>
                                    <input type="text" class="form-control" id="metaTitle">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="metaDescription">Meta description:</label>
                                    <textarea class="form-control" id="metaDescription"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="metaURL">Meta URL:</label>
                                    <input type="text" class="form-control" id="metaURL">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="metaKeywords">Meta keywords:</label>
                                    <input type="text" class="form-control" id="metaKeywords">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="saveItem">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- modalne okno zmena poradia -->
<div class="modal bs-example-modal-lg" tabindex="-1" role="dialog" id="modalConfirmReorder">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body confirm-dnd">
                <i class="fa fa-question-circle"></i>
                <p id="modalConfirmReorderQuestion"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="buttonDndReorderCancel">No</button>
                <button type="button" class="btn btn-danger" id="buttonDndReorderSuccess">Yes</button>
            </div>
        </div>
    </div>
</div>

<!-- modalne okno drag and drop -->
<div class="modal bs-example-modal-lg" tabindex="-1" role="dialog" id="modalConfirmDnd">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body confirm-dnd">
                <i class="fa fa-question-circle"></i>
                <p>Please confirm</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" id="buttonDndCancel">Cancel</button>

                <button type="button" class="btn btn-default" id="buttonDndCopy">Copy</button>
                <button type="button" class="btn btn-danger" id="buttonDndMove">Move</button>
            </div>
        </div>
    </div>
</div>

<!-- okno pre potvrdenie (confirm) -->
<div class="modal bs-example-modal-lg" tabindex="-1" role="dialog" id="modalConfirm">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body confirm-dialog">
                <i class="fa fa-exclamation-triangle"></i>
                <p id="modalConfirmQuestion"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger" id="modalConfirmSuccess"Yes</button>
            </div>
        </div>
    </div>
</div>

<!-- okno s upozornenim -->
<div class="modal bs-example-modal-lg" tabindex="-1" role="dialog" id="modalAlert">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Warning!!!</h4>
            </div>
            <div class="modal-body alert-dialog">
                <i class="fa fa-exclamation-triangle"></i>
                <p id="modalWarningMessage"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>