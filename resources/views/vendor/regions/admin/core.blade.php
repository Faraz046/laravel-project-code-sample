@extends('core::admin.master')

@section('title', $title)

@section('main')

@include('regions::admin._index')

@endsection
