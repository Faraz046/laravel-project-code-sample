@section('js')
    <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('js/admin/form.js') }}"></script>
@endsection

@include('core::admin._buttons-form')

{!! BootForm::hidden('id') !!}

{!! BootForm::text(trans('validation.attributes.company_name'), 'name') !!}

<div class="form-group">
    <h5>Assign users to company</h5>
    @foreach($users as $user)
        <label for="users[{{ $user->id  }}]">{{ $user->name }}</label>
        <input
                id="users[{{ $user->id  }}]"
                type="checkbox"
                name="users[]"
                value="{{ $user->id }}"
                {{ (! is_null($model) && in_array($user->id, $model->users->lists('id')->toArray())) ? 'checked' : ''  }}
        />
        <br/>
    @endforeach
</div>
