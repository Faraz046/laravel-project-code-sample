<li>
    <a href="{{ route($lang.'.companies.slug', $company->slug) }}" title="{{ $company->title }}">
        {!! $company->title !!}
        {!! $company->present()->thumb(null, 200) !!}
    </a>
</li>
