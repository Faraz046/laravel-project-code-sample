<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMetaColumnsToNewsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news_translations', function(Blueprint $table)
        {
            $table->string('meta_keywords')->nullable()->after('title');
            $table->string('meta_description')->nullable()->after('meta_keywords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_translations', function(Blueprint $table)
        {
            $table->dropColumn('meta_keywords');
            $table->dropColumn('meta_description');
        });
    }
}
