<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAirportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airports', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('company_id')->unsigned()->index()->nullable();
            $table->string('code')->nullable();
            $table->boolean('active')->default(true);
            $table->integer('position')->unsigned();
            $table->string('image')->nullable();
            $table->timestamps();
        });

        Schema::create('airport_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('airport_id')->unsigned();
            $table->string('locale');
            $table->boolean('status')->default(0);
            $table->string('title');
            $table->string('slug')->nullable();
            $table->timestamps();
            $table->unique(['airport_id', 'locale']);
            $table->unique(['locale', 'slug', 'airport_id']);
            $table->foreign('airport_id')->references('id')->on('airports')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('airport_translations');
        Schema::drop('airports');
    }
}
